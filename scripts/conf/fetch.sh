wget -N https://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/extract/bestuurlijkegrenzen.zip?datum=2020-01-10 --content-disposition

unzip -o bestuurlijkegrenzen.zip Gemeentegrenzen.gml
unzip -o bestuurlijkegrenzen.zip Provinciegrenzen.gml
rm bestuurlijkegrenzen.zip
