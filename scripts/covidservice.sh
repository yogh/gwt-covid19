echo "Setting root to: $1"

conf=$(pwd)/conf/covid-service.yml
log=$(pwd)/conf/logback.xml

cd ..
echo "Starting in $(pwd)"
mvn clean install -pl :covid19-service

while true; do
  mvn -pl :covid19-service \
    exec:exec \
    -Dserver.port=8081 \
    -Dexec.logback.args=-Dlogback.configurationFile=$log \
    -Dexec.micronaut.args="-Dmicronaut.config.files=$conf"

  sleep 1s
done
