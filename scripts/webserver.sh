cd ..

mkdir -p target/gwt/launcherDir > /dev/null

while true; do

if [ "$1" = "reload" ]; then
  mvn jetty:run -pl :covid19-server -Denv=dev-reload
else
  mvn jetty:run -pl :covid19-server -Denv=dev
fi
sleep 1s

done
