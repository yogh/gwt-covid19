package nl.yogh.covid.service.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.machinezoo.noexception.Exceptions;

import nl.yogh.covid.service.repository.AreaUtil;
import nl.yogh.covid.service.repository.InfectionRepository;
import nl.yogh.covid.service.repository.MunicipalityRepository;

public class InfectionReader {
  private static final Logger LOG = LoggerFactory.getLogger(InfectionReader.class);

  @Inject MunicipalityRepository municipalityRepo;
  @Inject InfectionRepository infectionRepo;

  public void go(final ApplicationConfiguration cfg) throws IOException {
    Files.list(Paths.get(cfg.getCases()))
        .filter(v -> v.getFileName().toString().endsWith(".csv"))
        .forEach(Exceptions.sneak().consumer(this::consume));
  }

  public void consume(final Path path) throws IOException {
    final String name = path.getFileName().toString();
    if (infectionRepo.hasMunicipalityCases(name)) {
      return;
    }

    try {
      final String dateText = name.split("_")[1].substring(6, 14);

      int dayy = Integer.parseInt(dateText.substring(0, 2));
      int mont = Integer.parseInt(dateText.substring(2, 4));
      int year = Integer.parseInt(dateText.substring(4));

      final Map<String, Integer> empty = municipalityRepo.getMunicipalities().stream()
          .map(v -> v.code())
          .collect(Collectors.toMap(v -> v, v -> 0));

      final Map<String, Integer> map = new HashMap<>(empty);
      if (name.startsWith("klik")) {
        map.putAll(Files.lines(path)
            .skip(1)
            .map(line -> line.split(";"))
            .collect(Collectors.toMap(v -> findMunicipalityCode(v[0]), v -> v.length == 4 ? Integer.parseInt(v[3]) : 0)));
      } else if (name.startsWith("nieuw-v2")) {
        map.putAll(Files.lines(path)
            .skip(1)
            .map(line -> line.split(";"))
            .map(parts -> new String[] { findMunicipalityCodeFromName(parts[0]), parts[2] })
            .filter(parts -> parts[0] != null)
            .collect(Collectors.toMap(v -> v[0], v -> Integer.parseInt(v[1]))));
      } else if (name.startsWith("nieuw-v3")) {
        year = Integer.parseInt(dateText.substring(0, 4));
        mont = Integer.parseInt(dateText.substring(4, 6));
        dayy = Integer.parseInt(dateText.substring(6, 8));

        map.putAll(Files.lines(path)
            .skip(1)
            .map(line -> line.split(";"))
            .map(parts -> new String[] { findMunicipalityCodeFromName(parts[0]), parts[4] })
            .filter(parts -> parts[0] != null)
            .collect(Collectors.toMap(v -> v[0], v -> Integer.parseInt(v[1]))));
      } else if (name.startsWith("nieuw-v4")) {
        year = Integer.parseInt(dateText.substring(0, 4));
        mont = Integer.parseInt(dateText.substring(4, 6));
        dayy = Integer.parseInt(dateText.substring(6, 8));

        map.putAll(Files.lines(path)
            .skip(1)
            .map(line -> line.split(";"))
            .map(parts -> new String[] { findMunicipalityCodeFromName(parts[0]), parts[1] })
            .filter(parts -> parts[0] != null)
            .collect(Collectors.toMap(v -> v[0], v -> Integer.parseInt(v[1]))));
      } else if (name.startsWith("v5")) {
        year = Integer.parseInt(dateText.substring(0, 4));
        mont = Integer.parseInt(dateText.substring(4, 6));
        dayy = Integer.parseInt(dateText.substring(6, 8));

        map.putAll(Files.lines(path)
            .map(line -> line.split(","))
            .map(parts -> new String[] { parts[0], parts[1] })
            .filter(parts -> parts[0] != null)
            .collect(Collectors.toMap(v -> v[0], v -> Integer.parseInt(v[1]))));
      } else if (name.startsWith("interp-lin-v1")) {
        year = Integer.parseInt(dateText.substring(0, 4));
        mont = Integer.parseInt(dateText.substring(4, 6));
        dayy = Integer.parseInt(dateText.substring(6, 8));

        map.putAll(Files.lines(path)
            .map(line -> line.split(";"))
            .map(parts -> new String[] { findMunicipalityCodeFromName(parts[0]), parts[1] })
            .filter(parts -> parts[0] != null)
            .collect(Collectors.toMap(v -> v[0], v -> Integer.parseInt(v[1]))));
      } else {
        map.putAll(Files.lines(path)
            .skip(1)
            .map(line -> line.split(";"))
            .map(parts -> new String[] { findMunicipalityCodeFromName(parts[0]), parts[1] })
            .filter(parts -> parts[0] != null)
            .collect(Collectors.toMap(v -> v[0], v -> Integer.parseInt(v[1]))));
      }

      final String[] nameParts = name.split("_");
      final String totalText = nameParts[nameParts.length - 1].split("\\.", 2)[0];

      Integer total;
      try {
        total = Integer.parseInt(totalText);
      } catch (final NumberFormatException e) {
        total = map.values().stream().mapToInt(v -> v).sum();
        LOG.info("Cannot parse totals. Falling back to summed up total: {}", total);
      }

      final Date date = new Date(year - 1900, mont - 1, dayy);
      infectionRepo.registerMunicipalityCases(name, date, total, map);
    } catch (final Exception e) {
      LOG.error("Error while trying to parse case file: {}", name, e);
    }
  }

  private String findMunicipalityCodeFromName(final String name) {
    final String cleanedName = AreaUtil.cleanName(name);

    final String code = municipalityRepo.retrieveMunicipalityCodeByName(cleanedName);

    if (code == null) {
      LOG.error("No code for name: [{}]", cleanedName);
    }

    return code;
  }

  private String findMunicipalityCode(final String codePlain) {
    final String cleaned = codePlain.indexOf(",") > -1 ? codePlain.substring(0, codePlain.indexOf(",")) : codePlain;

    return prependZeroes(Integer.parseInt(cleaned));
  }

  private String prependZeroes(final Integer num) {
    return String.format("%04d", num);
  }
}
