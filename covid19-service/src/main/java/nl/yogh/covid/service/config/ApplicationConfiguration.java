package nl.yogh.covid.service.config;

import javax.validation.constraints.NotBlank;

import io.micronaut.context.annotation.ConfigurationProperties;
import io.micronaut.context.annotation.Context;
import io.micronaut.core.annotation.Introspected;

@Context
@Introspected
@ConfigurationProperties("covid")
public class ApplicationConfiguration {
  @NotBlank private String provinceBoundaries;
  @NotBlank private String municipalityBoundaries;

  @NotBlank private String municipalities;

  @NotBlank private String cases;
  @NotBlank private String hospitalisations;
  @NotBlank private String fatalities;

  @NotBlank private String populations;

  @NotBlank private String updateEndpoint;
  @NotBlank private String updateTarget;
  private String updatePostrun;

  public String getMunicipalityBoundaries() {
    return municipalityBoundaries;
  }

  public void setMunicipalityBoundaries(final String municipalities) {
    this.municipalityBoundaries = municipalities;
  }

  public String getProvinceBoundaries() {
    return provinceBoundaries;
  }

  public void setProvinceBoundaries(final String provinces) {
    this.provinceBoundaries = provinces;
  }

  public String getCases() {
    return cases;
  }

  public void setCases(final String cases) {
    this.cases = cases;
  }

  public String getPopulations() {
    return populations;
  }

  public void setPopulations(final String populations) {
    this.populations = populations;
  }

  public String getMunicipalities() {
    return municipalities;
  }

  public void setMunicipalities(final String municipalities) {
    this.municipalities = municipalities;
  }

  public String getHospitalisations() {
    return hospitalisations;
  }

  public void setHospitalisations(final String hospitalisations) {
    this.hospitalisations = hospitalisations;
  }

  public void setFatalities(final String fatalities) {
    this.fatalities = fatalities;
  }

  public String getFatalities() {
    return fatalities;
  }

  @Override
  public String toString() {
    return "ApplicationConfiguration [municipalities=" + municipalityBoundaries + "]";
  }

  public String getUpdateEndpoint() {
    return updateEndpoint;
  }

  public void setUpdateEndpoint(final String updateEndpoint) {
    this.updateEndpoint = updateEndpoint;
  }

  public String getUpdateTarget() {
    return updateTarget;
  }

  public void setUpdateTarget(final String updateTarget) {
    this.updateTarget = updateTarget;
  }

  public String getUpdatePostrun() {
    return updatePostrun;
  }

  public void setUpdatePostrun(final String updatePostrun) {
    this.updatePostrun = updatePostrun;
  }
}
