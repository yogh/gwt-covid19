package nl.yogh.covid.service.config;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.tree.DefaultElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.simplify.DouglasPeuckerSimplifier;

import nl.yogh.covid.domain.MunicipalityInfo;
import nl.yogh.covid.service.repository.AreaUtil;
import nl.yogh.covid.service.repository.MunicipalityRepository;

public class MunicipalityReader {
  private static final Logger LOG = LoggerFactory.getLogger(MunicipalityReader.class);

  private static final int SRID_RDNEW = 28992;

  private final GeometryFactory geometryFactory;

  @Inject MunicipalityRepository repo;

  public MunicipalityReader() {
    geometryFactory = new GeometryFactory(new PrecisionModel(), SRID_RDNEW);
  }

  public void go(final ApplicationConfiguration cfg) {
    try {
      final Map<String, Integer> populations = Files.lines(Paths.get(cfg.getPopulations()))
          .map(v -> v.split(",", 2))
          .map(v -> new String[] { AreaUtil.cleanName(v[0]), v[1] })
          .collect(Collectors.toMap(v -> v[0], v -> Integer.parseInt(v[1])));

      final InputStream stream = Files.newInputStream(Paths.get(cfg.getMunicipalityBoundaries()));

      final SAXReader reader = new SAXReader();
      final Document document = reader.read(stream);

      final Element rootElem = document.getRootElement();

      final AtomicInteger failCounter = new AtomicInteger();
      ((List<DefaultElement>) rootElem.elements()).forEach(elem -> {
        if (!elem.getName().equals("featureMember")) {
          return;
        }

        final Element gemeente = elem.element("Gemeenten");
        final String code = gemeente.element("Code").getText();
        final String name = gemeente.element("Gemeentenaam").getText();
        final String nameKey = AreaUtil.cleanName(name);

        Polygon lineString = null;
        try {
          lineString = getLineString(gemeente);
//          LOG.info("Gemeente: {} > {} > {} -> {}", code, name, nameKey, populations.get(nameKey));

          final DouglasPeuckerSimplifier simplifier = new DouglasPeuckerSimplifier(lineString);
          simplifier.setDistanceTolerance(50D);

          final Geometry resultGeometry = simplifier.getResultGeometry();

          final Polygon resultPolygon = (Polygon) resultGeometry;

//          LOG.info("Before: {} After: {} Savings {}", lineString.toText().length(), resultPolygon.toText().length(),
//              (double) lineString.toText().length() / (double) resultPolygon.toText().length());

          repo.insertMunicipality(code, MunicipalityInfo.builder()
              .name(name)
              .code(code)
              .population(populations.get(nameKey))
              .geometry(resultPolygon.toText())
              .build());
        } catch (final Exception e) {
          failCounter.incrementAndGet();
          e.printStackTrace();
        }
      });

      LOG.info("Failures: {}", failCounter.get());
    } catch (final IOException | DocumentException e) {
      e.printStackTrace();
    }
  }

  private Polygon getLineString(final Element gemeente) {
    String feature = null;

    final Element multiSurfaceProperty = gemeente.element("multiSurfaceProperty");
    if (multiSurfaceProperty != null) {
      // This is amsterdam
      final Element ring = multiSurfaceProperty.element("MultiSurface").element("surfaceMember").element("Surface").element("patches")
          .element("PolygonPatch").element("exterior").element("LinearRing");

      final String stringValue = ring.getStringValue();

      if (feature == null || feature.length() < stringValue.length()) {
        feature = stringValue.trim();
      }
    }

    final Element featureGml = gemeente.element("surfaceProperty");
    if (featureGml != null) {
      final Element ring = featureGml
          .element("Surface")
          .element("patches")
          .element("PolygonPatch")
          .element("exterior")
          .element("LinearRing");

      final String stringValue = ring.getStringValue();
      feature = stringValue.trim();
    }

    final Coordinate[] coordinates = fromString(feature);

    if (coordinates.length > 0 && !coordinates[0].equals(coordinates[coordinates.length - 1])) {
      LOG.error("Geometry is NOT fine. Fixing.");
      coordinates[coordinates.length - 1] = coordinates[0];
    }

    return geometryFactory.createPolygon(coordinates);
  }

  private Coordinate[] fromString(final String feature) {
    String corrected = feature
        .replaceAll("\n", "")
        .replaceAll("\t", "");

    while (corrected.indexOf("  ") > -1) {
      corrected = corrected.replaceAll("  ", " ");
    }

    final String[] parts = corrected.split(" ");
    final Coordinate[] coordinates = new Coordinate[parts.length / 2];

    for (int i = 0; i < parts.length; i += 2) {
      if (i + 1 >= parts.length) {
        continue;
      }

      try {
        coordinates[i / 2] = new Coordinate(Double.parseDouble(parts[i]), Double.parseDouble(parts[i + 1]));
      } catch (final NumberFormatException e) {
        LOG.info("Parsing: {} > {}", parts[i], parts[i + 1]);
        continue;
      }
    }

    return coordinates;
  }

}
