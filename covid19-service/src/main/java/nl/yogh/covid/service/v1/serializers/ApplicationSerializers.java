package nl.yogh.covid.service.v1.serializers;

import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;

import nl.yogh.covid.domain.CasesDistributionSetJsonSerializer;
import nl.yogh.covid.domain.FatalityDistributionSetJsonSerializer;
import nl.yogh.covid.domain.FatalityInfoJsonSerializer;
import nl.yogh.covid.domain.HospitalisationDistributionSetJsonSerializer;
import nl.yogh.covid.domain.HospitalisationInfoJsonSerializer;
import nl.yogh.covid.domain.MunicipalityInfoJsonSerializer;
import nl.yogh.covid.domain.ProvinceInfoJsonSerializer;
import nl.yogh.covid.domain.VirusDistributionInfoJsonSerializer;
import nl.yogh.covid.domain.VirusInfoJsonSerializer;

@Factory
public class ApplicationSerializers {
  @Bean
  public ProvinceInfoJsonSerializer getProvinceInfo() {
    return new ProvinceInfoJsonSerializer();
  }

  @Bean
  public VirusDistributionInfoJsonSerializer getVirusDistributionInfo() {
    return new VirusDistributionInfoJsonSerializer();
  }

  @Bean
  public CasesDistributionSetJsonSerializer getCasesDistributionSetJsonSerializer() {
    return new CasesDistributionSetJsonSerializer();
  }

  @Bean
  public HospitalisationDistributionSetJsonSerializer getHospitalisationDistributionSet() {
    return new HospitalisationDistributionSetJsonSerializer();
  }

  @Bean
  public FatalityDistributionSetJsonSerializer getFatalityDistributionSetJsonSerializer() {
    return new FatalityDistributionSetJsonSerializer();
  }

  @Bean
  public MunicipalityInfoJsonSerializer getMunicipalityInfo() {
    return new MunicipalityInfoJsonSerializer();
  }

  @Bean
  public VirusInfoJsonSerializer getVirusInfo() {
    return new VirusInfoJsonSerializer();
  }

  @Bean
  public FatalityInfoJsonSerializer getFatalityInfo() {
    return new FatalityInfoJsonSerializer();
  }

  @Bean
  public HospitalisationInfoJsonSerializer getHospitalisationInfo() {
    return new HospitalisationInfoJsonSerializer();
  }
}
