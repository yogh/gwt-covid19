package nl.yogh.covid.service.repository;

public class AreaUtil {
  public static String cleanName(final String name) {
    final String cleaned = name
        .toLowerCase()
        .replace(".", "")
        .replaceAll("\"", "")
        .replaceAll(",", "")
        .replaceAll("'", "")
        .replaceAll(" ", "")
        .replaceAll("-", "")
        .replaceAll("ã¢", "â")
        .replaceAll("ãº", "ú")
        .intern();
    
    if ("hengelo".equals(cleaned)) {
      return "hengelo(o)";
    }

    return cleaned;
  }
}
