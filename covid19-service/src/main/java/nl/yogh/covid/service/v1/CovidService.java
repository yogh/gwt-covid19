package nl.yogh.covid.service.v1;

import java.util.ArrayList;
import java.util.stream.Collectors;

import javax.inject.Inject;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

import nl.yogh.covid.domain.RequestMapping;
import nl.yogh.covid.domain.VirusDistributionInfo;
import nl.yogh.covid.service.repository.InfectionRepository;
import nl.yogh.covid.service.repository.MunicipalityRepository;
import nl.yogh.covid.service.repository.ProvinceRepository;

@Controller("/v1/")
public class CovidService {
  private static final long LIMIT = 150;

  @Inject InfectionRepository infectionRepo;

  @Inject MunicipalityRepository municipalityRepo;
  @Inject ProvinceRepository provinceRepo;

  @Get(RequestMapping.GET_DISTRIBUTION)
  public VirusDistributionInfo retrieveMunicipalityInfo() {
    return VirusDistributionInfo.builder()
        .cases(infectionRepo.getMunicipalityCaseDistribution())
        .hospitalisations(infectionRepo.getMunicipalityHospitalisationDistribution())
        .fatalities(infectionRepo.getMunicipalityFatalitiesDistribution())
        .municipalities(municipalityRepo.getMunicipalities())
        .provinces(provinceRepo.getProvinces())
        .build();
  }

  @Get(RequestMapping.GET_DISTRIBUTION_LIMIT)
  public VirusDistributionInfo retrieveMunicipalityInfoLimited() {
    return VirusDistributionInfo.builder()
        .cases(limit(infectionRepo.getMunicipalityCaseDistribution()))
        .hospitalisations(limit(infectionRepo.getMunicipalityHospitalisationDistribution()))
        .fatalities(limit(infectionRepo.getMunicipalityFatalitiesDistribution()))
        .municipalities(municipalityRepo.getMunicipalities())
        .provinces(provinceRepo.getProvinces())
        .build();
  }

  private <T> ArrayList<T> limit(final ArrayList<T> lst) {
    final int skip = (int) Math.max(0, lst.size() - LIMIT);
    
    return lst.stream()
        .skip(skip)
        .collect(Collectors.toCollection(ArrayList::new));
  }
}
