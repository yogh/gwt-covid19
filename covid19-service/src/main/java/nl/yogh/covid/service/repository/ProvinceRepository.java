package nl.yogh.covid.service.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.yogh.covid.domain.ProvinceInfo;

@Singleton
public class ProvinceRepository {
  private static final Logger LOG = LoggerFactory.getLogger(ProvinceRepository.class);

  private final Map<String, ProvinceInfo> provinces = new HashMap<>();
  private final Map<String, List<String>> municipalityMappings = new HashMap<>();

  private final Map<String, String> reverseMunicipalityMappings = new HashMap<>();

  public void insertProvince(final String code, final ProvinceInfo bldr) {
    provinces.put(code, bldr);
  }

  public String retrieveProvinceForMunicipality(final String municipalityCode) {
    return reverseMunicipalityMappings.get(municipalityCode);
  }

  public ArrayList<ProvinceInfo> getProvinces() {
    return new ArrayList<>(provinces.values());
  }

  public void registerMunicipalitymapping(final String provinceKey, final String municipalityCode) {
    municipalityMappings.merge(provinceKey, List.of(municipalityCode),
        (o, n) -> Stream.concat(o.stream(), n.stream()).collect(Collectors.toList()));
    reverseMunicipalityMappings.put(municipalityCode, provinceKey);
  }

  public List<String> retrieveMunicipalities(final String nameKey) {
    return municipalityMappings.get(nameKey);
  }
}
