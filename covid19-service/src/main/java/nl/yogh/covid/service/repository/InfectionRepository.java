package nl.yogh.covid.service.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.yogh.covid.domain.CasesDistributionSet;
import nl.yogh.covid.domain.FatalityDistributionSet;
import nl.yogh.covid.domain.FatalityInfo;
import nl.yogh.covid.domain.HospitalisationDistributionSet;
import nl.yogh.covid.domain.HospitalisationInfo;
import nl.yogh.covid.domain.VirusInfo;

@Singleton
public class InfectionRepository {
  private static final Logger LOG = LoggerFactory.getLogger(InfectionRepository.class);

  @Inject ProvinceRepository provinceRepository;

  private final Map<String, CasesDistributionSet> municipalityCases = new HashMap<>();

  private final Map<String, HospitalisationDistributionSet> municipalityHospitalisations = new HashMap<>();
  private final Map<String, FatalityDistributionSet> municipalityFatalities = new HashMap<>();

  public ArrayList<CasesDistributionSet> getMunicipalityCaseDistribution() {
    final ArrayList<CasesDistributionSet> lst = new ArrayList<>(municipalityCases.values());
    lst.sort((o1, o2) -> Long.compare(o1.date(), o2.date()));

    return lst;
  }

  public ArrayList<HospitalisationDistributionSet> getMunicipalityHospitalisationDistribution() {
    final ArrayList<HospitalisationDistributionSet> lst = new ArrayList<>(municipalityHospitalisations.values());
    lst.sort((o1, o2) -> Long.compare(o1.date(), o2.date()));

    return lst;
  }

  public ArrayList<FatalityDistributionSet> getMunicipalityFatalitiesDistribution() {
    final ArrayList<FatalityDistributionSet> lst = new ArrayList<>(municipalityFatalities.values());
    lst.sort((o1, o2) -> Long.compare(o1.date(), o2.date()));

    return lst;
  }

  public void registerMunicipalityCases(final String name, final Date date, final Integer total, final Map<String, Integer> caseDistribution) {
    if (municipalityCases.containsKey(name)) {
      return;
    }

    LOG.info("Registering #cases dataset {} with {} municipalities", name, caseDistribution.size());

    municipalityCases.put(name, CasesDistributionSet.builder()
        .source(name)
        .total(total)
        .date(date.getTime() / 1000)
        .provinceDistribution(caseDistribution.entrySet().stream()
            .filter(v -> v.getKey() != null)
            .filter(v -> provinceRepository.retrieveProvinceForMunicipality(v.getKey()) != null)
            .collect(Collectors.groupingBy(k -> provinceRepository.retrieveProvinceForMunicipality(k.getKey()),
                Collectors.mapping(v -> v.getValue(), Collectors.toList())))
            .entrySet().stream()
            .map(e -> VirusInfo.builder()
                .code(e.getKey())
                .amount(e.getValue().stream().mapToInt(v -> v).sum())
                .build())
            .collect(Collectors.toCollection(ArrayList::new)))
        .municipalityDistribution(caseDistribution.entrySet().stream()
            .map(e -> VirusInfo.builder()
                .code(e.getKey())
                .amount(e.getValue())
                .build())
            .collect(Collectors.toCollection(ArrayList::new)))
        .build());
  }

  public void registerMunicipalityHospitalisations(final String name, final Date date, final Integer total,
      final Map<String, Integer> hospitalistations) {
    if (municipalityHospitalisations.containsKey(name)) {
      return;
    }

    LOG.info("Registering #hospitalistations dataset {} with {} municipalities", name, hospitalistations.size());

    municipalityHospitalisations.put(name, HospitalisationDistributionSet.builder()
        .source(name)
        .total(total)
        .date(date.getTime() / 1000)
        .provinceDistribution(hospitalistations.entrySet().stream()
            .filter(v -> v.getKey() != null)
            .filter(v -> provinceRepository.retrieveProvinceForMunicipality(v.getKey()) != null)
            .collect(Collectors.groupingBy(k -> provinceRepository.retrieveProvinceForMunicipality(k.getKey()),
                Collectors.mapping(v -> v.getValue(), Collectors.toList())))
            .entrySet().stream()
            .map(e -> HospitalisationInfo.builder()
                .code(e.getKey())
                .amount(e.getValue().stream().mapToInt(v -> v).sum())
                .build())
            .collect(Collectors.toCollection(ArrayList::new)))
        .municipalityDistribution(hospitalistations.entrySet().stream()
            .map(e -> HospitalisationInfo.builder()
                .code(e.getKey())
                .amount(e.getValue())
                .build())
            .collect(Collectors.toCollection(ArrayList::new)))
        .build());
  }

  public void registerMunicipalityFatalities(final String name, final Date date, final Integer total,
      final Map<String, Integer> fatalities) {
    if (municipalityFatalities.containsKey(name)) {
      return;
    }

    LOG.info("Registering #fatalities dataset {} with {} municipalities", name, fatalities.size());

    municipalityFatalities.put(name, FatalityDistributionSet.builder()
        .source(name)
        .total(total)
        .date(date.getTime() / 1000)
        .provinceDistribution(fatalities.entrySet().stream()
            .filter(v -> v.getKey() != null)
            .filter(v -> provinceRepository.retrieveProvinceForMunicipality(v.getKey()) != null)
            .collect(Collectors.groupingBy(k -> provinceRepository.retrieveProvinceForMunicipality(k.getKey()),
                Collectors.mapping(v -> v.getValue(), Collectors.toList())))
            .entrySet().stream()
            .map(e -> FatalityInfo.builder()
                .code(e.getKey())
                .amount(e.getValue().stream().mapToInt(v -> v).sum())
                .build())
            .collect(Collectors.toCollection(ArrayList::new)))
        .municipalityDistribution(fatalities.entrySet().stream()
            .map(e -> FatalityInfo.builder()
                .code(e.getKey())
                .amount(e.getValue())
                .build())
            .collect(Collectors.toCollection(ArrayList::new)))
        .build());
  }

  public boolean hasMunicipalityCases(final String name) {
    return municipalityCases.containsKey(name);
  }

  public boolean hasMunicipalityHospitalisations(final String name) {
    return municipalityHospitalisations.containsKey(name);
  }

  public boolean hasMunicipalityFatalities(final String name) {
    return municipalityFatalities.containsKey(name);
  }
}
