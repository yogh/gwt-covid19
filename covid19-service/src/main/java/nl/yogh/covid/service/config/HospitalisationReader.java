package nl.yogh.covid.service.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.machinezoo.noexception.Exceptions;

import nl.yogh.covid.service.repository.AreaUtil;
import nl.yogh.covid.service.repository.InfectionRepository;
import nl.yogh.covid.service.repository.MunicipalityRepository;

public class HospitalisationReader {
  private static final Logger LOG = LoggerFactory.getLogger(HospitalisationReader.class);

  @Inject MunicipalityRepository municipalityRepo;
  @Inject InfectionRepository infectionRepo;

  public void go(final ApplicationConfiguration cfg) throws IOException {
    Files.list(Paths.get(cfg.getHospitalisations()))
        .filter(v -> v.getFileName().toString().endsWith(".csv"))
        .forEach(Exceptions.sneak().consumer(this::consume));
  }

  public void consume(final Path path) throws IOException {
    final String name = path.getFileName().toString();
    if (infectionRepo.hasMunicipalityHospitalisations(name)) {
      return;
    }

    try {
      final String dateText = name.split("_")[1].substring(17, 25);

      final int dayy = Integer.parseInt(dateText.substring(6, 8));
      final int mont = Integer.parseInt(dateText.substring(4, 6));
      final int year = Integer.parseInt(dateText.substring(0, 4));

      final Date date = new Date(year - 1900, mont - 1, dayy);

      final Map<String, Integer> empty = municipalityRepo.getMunicipalities().stream()
          .map(v -> v.code())
          .collect(Collectors.toMap(v -> v, v -> 0));

      final Map<String, Integer> map = new HashMap<>(empty);
      if (name.startsWith("v1")) {
        map.putAll(Files.lines(path)
            .skip(1)
            .map(line -> line.split(";"))
            .map(parts -> new String[] { findMunicipalityCodeFromName(parts[0]), calculateCases(parts[1], parts[2]) })
            .filter(parts -> parts[0] != null)
            .collect(Collectors.toMap(v -> v[0], v -> Integer.parseInt(v[1]))));
      } else if (name.startsWith("v2")) {
        map.putAll(Files.lines(path)
            .skip(1)
            .map(line -> line.split(";"))
            .map(parts -> new String[] { findMunicipalityCodeFromName(parts[0]), parts[2] })
            .filter(parts -> parts[0] != null)
            .collect(Collectors.toMap(v -> v[0], v -> (int) Double.parseDouble(v[1]))));
      } else if (name.startsWith("v3")) {
        map.putAll(Files.lines(path)
            .skip(1)
            .map(line -> line.split(";"))
            .map(parts -> new String[] { findMunicipalityCodeFromName(parts[0]), parts[2] })
            .filter(parts -> parts[0] != null)
            .collect(Collectors.toMap(v -> v[0], v -> (int) Double.parseDouble(v[1]))));
      } else if (name.startsWith("v4")) {
        map.putAll(Files.lines(path)
            .skip(1)
            .map(line -> line.split(";"))
            .map(parts -> new String[] { findMunicipalityCodeFromName(parts[0]), parts[3] })
            .filter(parts -> parts[0] != null)
            .collect(Collectors.toMap(v -> v[0], v -> (int) Double.parseDouble(v[1]))));
      } else if (name.startsWith("v5")) {
        map.putAll(Files.lines(path)
            .map(line -> line.split(","))
            .map(parts -> new String[] { parts[0], parts[2] })
            .filter(parts -> parts[0] != null)
            .collect(Collectors.toMap(v -> v[0], v -> Integer.parseInt(v[1]))));
      }

      final String[] nameParts = name.split("_");
      final String totalText = nameParts[nameParts.length - 1].split("\\.", 2)[0];

      Integer total;
      try {
        total = Integer.parseInt(totalText);
      } catch (final NumberFormatException e) {
        total = map.values().stream().mapToInt(v -> v).sum();
        LOG.info("Cannot parse totals. Falling back to summed up total: {}", total);
      }

      infectionRepo.registerMunicipalityHospitalisations(name, date, total, map);
    } catch (final Exception e) {
      LOG.error("Error while trying to parse case file: {}", name, e);
    }
  }

  private String calculateCases(final String perHundredThousandStr, final String populationStr) {
    final int population = Integer.parseInt(populationStr);
    final double perHundredThousand = Double.parseDouble(perHundredThousandStr);
    final int total = (int) (population * (perHundredThousand / (100 * 1000D)));

    return String.valueOf(total);
  }

  private String findMunicipalityCodeFromName(final String name) {
    final String cleanedName = AreaUtil.cleanName(name);

    final String code = municipalityRepo.retrieveMunicipalityCodeByName(cleanedName);

    if (code == null) {
      LOG.error("No code for name: [{}]", cleanedName);
    }

    return code;
  }
}
