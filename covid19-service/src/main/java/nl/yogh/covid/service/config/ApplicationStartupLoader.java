package nl.yogh.covid.service.config;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micronaut.context.event.ApplicationEventListener;
import io.micronaut.discovery.event.ServiceStartedEvent;

import nl.yogh.covid.service.reloader.ReloaderService;
import nl.yogh.covid.service.repository.InfectionRepository;
import nl.yogh.covid.service.repository.MunicipalityRepository;
import nl.yogh.covid.service.repository.ProvinceRepository;

@Singleton
public class ApplicationStartupLoader implements ApplicationEventListener<ServiceStartedEvent> {
  private static final Logger LOG = LoggerFactory.getLogger(ApplicationStartupLoader.class);

  @Inject MunicipalityRepository municipalityRepo;
  @Inject InfectionRepository infectionRepository;
  @Inject ProvinceRepository provinceRepo;

  @Inject MunicipalityReader municipalityReader;
  @Inject ProvinceReader provinceReader;

  @Inject InfectionReader infectionReader;
  @Inject HospitalisationReader hospReader;
  @Inject FatalitiesReader fatalityReader;

  @Inject @Valid ApplicationConfiguration cfg;

  @Inject ReloaderService reloader;

  @Override
  public void onApplicationEvent(final ServiceStartedEvent event) {
    municipalityReader.go(cfg);
    provinceReader.go(cfg);

    indexFiles();

    LOG.info("Created entries for {} provinces.", provinceRepo.getProvinces().size());
    LOG.info("Created entries for {} municipalities.", municipalityRepo.getMunicipalities().size());

    reloader.go(cfg, () -> {
      indexFiles();
    });

    findMissing(() -> {
      indexFiles();
    });

    LOG.info("Initialized auto-reloader");
  }

  private void findMissing(final Runnable postUpdate) {
    final LocalDate first = LocalDate.of(2021, 1, 1);
    final LocalDate last = LocalDate.now().plusDays(1);

    LOG.info("Searching for missing dates and attempting retroactive indexation ({} - {}).", first, last);

    final Collection<LocalDate> allDates = first.datesUntil(last)
        .collect(Collectors.toCollection(ArrayList::new));

    final List<LocalDate> availableDates = infectionRepository.getMunicipalityCaseDistribution()
        .stream()
        .map(v -> new Date(v.date() * 1000))
        .map(v -> v.toInstant()
            .atZone(ZoneId.of("Europe/Paris"))
            .toLocalDate())
        .filter(v -> v.getYear() >= 2021)
        .collect(Collectors.toList());
    
    LOG.info("available/all dates: {}/{}", availableDates.size(), allDates.size());

    allDates.removeAll(availableDates);

    indexMissing(allDates, postUpdate);
  }

  private void indexMissing(final Collection<LocalDate> allDates, final Runnable postUpdate) {
    final ZoneId zone = ZoneId.of("Europe/Paris");
    allDates.forEach(v -> {
      LOG.info("Attempting retroactive indexation for date: {}", v);
      reloader.fetchData(cfg, null, ZonedDateTime.of(v.atTime(12, 0), zone));
    });
    
    if (!allDates.isEmpty()) {
      postUpdate.run();
    }
  }

  private void indexFiles() {
    try {
      final int infBefore = infectionRepository.getMunicipalityCaseDistribution().size();
      final int hosBefore = infectionRepository.getMunicipalityHospitalisationDistribution().size();
      final int fatBefore = infectionRepository.getMunicipalityFatalitiesDistribution().size();

      infectionReader.go(cfg);
      hospReader.go(cfg);
      fatalityReader.go(cfg);

      final int infAfter = infectionRepository.getMunicipalityCaseDistribution().size();
      final int hosAfter = infectionRepository.getMunicipalityHospitalisationDistribution().size();
      final int fatAfter = infectionRepository.getMunicipalityFatalitiesDistribution().size();

      LOG.info("Created entries for {} case# datasets.", infAfter - infBefore);
      LOG.info("Created entries for {} hosp# datasets.", hosAfter - hosBefore);
      LOG.info("Created entries for {} fatl# datasets.", fatAfter - fatBefore);
    } catch (final IOException e) {
      throw new RuntimeException(e);
    }
  }
}
