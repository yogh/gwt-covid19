package nl.yogh.covid.service.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.yogh.covid.domain.MunicipalityInfo;

@Singleton
public class MunicipalityRepository {
  private static final Logger LOG = LoggerFactory.getLogger(MunicipalityRepository.class);

  private final Map<String, MunicipalityInfo> municipalities = new HashMap<>();

  private final Map<String, String> municipalityCodesByName = new HashMap<>();

  public void insertMunicipality(final String code, final MunicipalityInfo bldr) {
    municipalityCodesByName.put(AreaUtil.cleanName(bldr.name()), code);
    municipalities.put(code, bldr);
  }

  public String retrieveMunicipalityCodeByName(final String name) {
    return municipalityCodesByName.get(AreaUtil.cleanName(name));
  }

  public MunicipalityInfo retrieveMunicipality(final String code) {
    return municipalities.get(code);
  }

  public ArrayList<MunicipalityInfo> getMunicipalities() {
    return new ArrayList<>(municipalities.values());
  }
}
