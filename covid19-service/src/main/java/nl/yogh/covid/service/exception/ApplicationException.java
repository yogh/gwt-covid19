package nl.yogh.covid.service.exception;

public class ApplicationException extends Exception {
  private static final long serialVersionUID = 3816359155180413320L;

  public ApplicationException(final String msg) {
    super(msg);
  }

  public ApplicationException(final String msg, final Throwable cause) {
    super(msg, cause);
  }
}
