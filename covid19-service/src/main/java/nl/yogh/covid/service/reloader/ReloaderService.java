package nl.yogh.covid.service.reloader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Files;

import nl.yogh.covid.service.config.ApplicationConfiguration;

import kong.unirest.Unirest;

@Singleton
public class ReloaderService {
  private static final Logger LOG = LoggerFactory.getLogger(ReloaderService.class);

  private static final DateTimeFormatter FORM_COMPACT = DateTimeFormatter.ofPattern("yyyyMMdd");
  private static final DateTimeFormatter FORM = DateTimeFormatter.ofPattern("yyyy-MM-dd");

  public void go(final ApplicationConfiguration cfg, final Runnable postUpdate) {
    final ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
    ZonedDateTime nextRun = now.withHour(15).withMinute(14).withSecond(40);
    if (now.compareTo(nextRun) > 0) {
      nextRun = nextRun.plusDays(1);
    }

    final Duration initalDelay = Duration.between(now, nextRun);

    final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    scheduler.scheduleAtFixedRate(() -> fetchData(cfg, postUpdate),
        initalDelay.getSeconds(),
        TimeUnit.DAYS.toSeconds(1),
        TimeUnit.SECONDS);

    LOG.info("Scheduled initial auto-update {} minutes ({} hours) from now.", initalDelay.get(ChronoUnit.SECONDS) / 60,
        initalDelay.get(ChronoUnit.SECONDS) / 60 / 60);
  }

  public void fetchData(final ApplicationConfiguration cfg, final Runnable postUpdate) {
    final ZonedDateTime day = ZonedDateTime.now(ZoneId.of("Europe/Paris"));

    fetchData(cfg, postUpdate, day);
  }

  public void fetchData(final ApplicationConfiguration cfg, final Runnable postUpdate, final ZonedDateTime dateTarget) {
    final String dateCompact = dateTarget.format(FORM_COMPACT);
    final String date = dateTarget.format(FORM);

    LOG.info("Performing auto-update. Fetching data for day: {} ({})", date, dateTarget);

    LOG.info("Getting {}", cfg.getUpdateEndpoint());
    final List<String[]> resp = Unirest.get(cfg.getUpdateEndpoint())
        .asString()
        .mapBody(v -> Stream.of(v.split("\r\n")))
        .map(v -> v.split(";"))
        .filter(v -> v[1].startsWith(date))
        .collect(Collectors.toList());

    final String out = resp.stream()
        .filter(v -> v[2] != null && v[2].startsWith("GM"))
        .map(v -> v[2].substring(2) + "," + v[5] + "," + v[6] + "," + v[7])
        .collect(Collectors.joining("\n"));

    final AtomicInteger casesSum = new AtomicInteger(0);
    final AtomicInteger hospiSum = new AtomicInteger(0);
    final AtomicInteger fatalSum = new AtomicInteger(0);
    resp.stream()
        .forEach(v -> {
          casesSum.addAndGet(Integer.parseInt(v[5]));
          hospiSum.addAndGet(Integer.parseInt(v[6]));
          fatalSum.addAndGet(Integer.parseInt(v[7]));
        });

    if (out.length() == 0) {
      LOG.warn("No new data entries discovered. Trying again in 10 seconds.");
      final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
      scheduler.schedule(() -> fetchData(cfg, postUpdate),
          10, TimeUnit.SECONDS);
      return;
    }

    Stream.of(cfg.getUpdateTarget().split(","))
        .map(v -> v
            .replaceAll("\\{date\\}", dateCompact)
            .replaceAll("\\{cases\\}", String.valueOf(casesSum.get()))
            .replaceAll("\\{hospitalisations\\}", String.valueOf(hospiSum.get()))
            .replaceAll("\\{fatalities\\}", String.valueOf(fatalSum.get())))
        .forEach(obj -> {
          if (obj == null || obj.isBlank()) {
            return;
          }

          try {
            LOG.info("Writing update to file: {}", obj);
            Files.write(out.getBytes(StandardCharsets.UTF_8), Paths.get(obj).toFile());
          } catch (final IOException e) {
            LOG.error("Could not finish write to target [{}] due to IOException.", obj, e);
          }
        });

    if (postUpdate != null) {
      LOG.info("Executing post-update file registrations.");
      postUpdate.run();
    }

    if (cfg.getUpdatePostrun() != null) {
      final String updatePostrun = cfg.getUpdatePostrun()
          .replaceAll("\\{date\\}", dateCompact);

      if (updatePostrun == null || updatePostrun.isBlank()) {
        LOG.info("Skipping postrun command");
        return;
      }

      LOG.info("Executing postcommand: {}", updatePostrun);
      final ProcessBuilder processBuilder = new ProcessBuilder();
      final ProcessBuilder command = processBuilder.command(updatePostrun);
      try {
        final Process process = command.start();
        final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        final StringBuilder builder = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
          builder.append(">>>> ");
          builder.append(line);
          builder.append(System.getProperty("line.separator"));
        }

        builder.setLength(builder.length() - System.getProperty("line.separator").length());
        final String result = builder.toString();
        LOG.info("Printing postcommand output:\n{}", result);
      } catch (final IOException e) {
        LOG.info("Exception while executing post command: {}", updatePostrun, e);
      }
    }
  }
}
