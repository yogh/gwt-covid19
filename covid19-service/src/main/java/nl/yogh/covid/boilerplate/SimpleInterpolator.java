package nl.yogh.covid.boilerplate;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.machinezoo.noexception.Exceptions;

import nl.yogh.covid.service.config.InfectionReader;

public class SimpleInterpolator {
  private static final Logger LOG = LoggerFactory.getLogger(InfectionReader.class);

  public static void main(final String[] args) throws IOException {
    final String a = args[0];
    final String b = args[1];
    final String destination = args[2];

    final LocalDate startExclusive = LocalDate.of(2020, 3, 30);
    final LocalDate endExclusive = LocalDate.of(2020, 4, 8);

    LOG.info("Writing to {}", destination);

    final Map<String, Integer> aMap = Files.lines(Paths.get(a))
        .skip(1)
        .map(v -> v.split(";"))
        .map(v -> new String[] { v[0], v[2] })
        .collect(Collectors.toMap(v -> v[0], v -> Integer.parseInt(v[1])));
    final int aTotals = findTotal(a);

    final Map<String, Integer> bMap = Files.lines(Paths.get(b))
        .skip(1)
        .map(v -> v.split(";"))
        .map(v -> new String[] { v[0], v[4] })
        .collect(Collectors.toMap(v -> v[0], v -> Integer.parseInt(v[1])));
    final int bTotals = findTotal(b);

    final int between = (int) ChronoUnit.DAYS.between(startExclusive, endExclusive);
    final int increments = (int) ChronoUnit.DAYS.between(startExclusive.plusDays(1), endExclusive);

    final LinearInterpolator totalInterpolator = new LinearInterpolator();
    final PolynomialSplineFunction totalFunc = totalInterpolator.interpolate(new double[] { 0, between }, new double[] { aTotals, bTotals });

    final Map<String, PolynomialSplineFunction> interpolatedMap = bMap.entrySet()
        .stream()
        .collect(Collectors.toMap(e -> e.getKey(), e -> {
          final int aCases = aMap.getOrDefault(e.getKey(), 0);
          final int bCases = e.getValue();

          final LinearInterpolator interpolator = new LinearInterpolator();
          final PolynomialSplineFunction func = interpolator.interpolate(
              new double[] { 0, between },
              new double[] { aCases, bCases });

          return func;
        }));

    Stream.iterate(startExclusive.plusDays(1), d -> d.plusDays(1))
        .limit(increments)
        .forEach(Exceptions.sneak().consumer(date -> {
          final long increment = ChronoUnit.DAYS.between(startExclusive, date);
          LOG.info("Generating increment: {}", increment);

          final int total = (int) Math.round(totalFunc.value(increment));
          LOG.info("Interpolated totals  : {}", total);

          final String destName = "interp-lin-v1_corona" + date.format(DateTimeFormatter.BASIC_ISO_DATE) + "_" + total + ".csv";
          final Path destFile = Files.write(Paths.get(destination).resolve(destName), new byte[] {}, StandardOpenOption.CREATE,
              StandardOpenOption.TRUNCATE_EXISTING);

          try (PrintWriter pw = new PrintWriter(destFile.toFile())) {
            interpolatedMap.forEach((k, v) -> pw.println(k + ";" + (int) Math.round(v.value(increment))));
          }

          LOG.info("Generating interpolated: {}", destName);
        }));
  }

  private static int findTotal(final String name) {
    final String[] nameParts = name.split("_");
    final String totalText = nameParts[nameParts.length - 1].split("\\.", 2)[0];
    Integer total;
    try {
      total = Integer.parseInt(totalText);
    } catch (final NumberFormatException e) {
      total = null;
    }
    return total;
  }
}
