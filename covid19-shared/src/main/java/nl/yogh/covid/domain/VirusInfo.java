package nl.yogh.covid.domain;

import java.io.Serializable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class VirusInfo implements Serializable, IndexedDistribution {
  private static final long serialVersionUID = 1L;

  public static Builder builder() {
    return new AutoValue_VirusInfo.Builder();
  }

  @Override
  public abstract String code();

  @Override
  public abstract int amount();

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder code(String value);

    public abstract Builder amount(int value);

    public abstract VirusInfo build();
  }
}
