package nl.yogh.covid.domain;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.Nullable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class FatalityDistributionSet implements Serializable {
  private static final long serialVersionUID = 1L;

  public static Builder builder() {
    return new AutoValue_FatalityDistributionSet.Builder();
  }

  public abstract String source();

  @Nullable
  public abstract Integer total();

  public abstract Long date();

  public abstract ArrayList<FatalityInfo> provinceDistribution();

  public abstract ArrayList<FatalityInfo> municipalityDistribution();

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder source(String value);

    public abstract Builder total(Integer value);

    public abstract Builder date(Long value);

    public abstract Builder provinceDistribution(ArrayList<FatalityInfo> value);

    public abstract Builder municipalityDistribution(ArrayList<FatalityInfo> value);

    public abstract FatalityDistributionSet build();
  }
}
