package nl.yogh.covid.domain;

import java.io.Serializable;

import javax.annotation.Nullable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class GrowthInfo implements Serializable, IndexedGrowthDistribution {
  private static final long serialVersionUID = 1L;

  public static Builder builder() {
    return new AutoValue_GrowthInfo.Builder();
  }

  @Override
  public abstract String code();

  @Nullable
  @Override
  public abstract Integer amount();

  @Override
  @Nullable
  public abstract Double factor();

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder code(String value);

    public abstract Builder amount(Integer value);

    public abstract Builder factor(Double value);

    public abstract GrowthInfo build();
  }
}
