package nl.yogh.covid.domain;

public class RequestMapping {
  public static final String GET_DISTRIBUTION = "distribution";
  public static final String GET_DISTRIBUTION_LIMIT = "distribution-limited";
}
