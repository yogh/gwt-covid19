package nl.yogh.covid.domain;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class VirusDistributionInfo implements Serializable {
  private static final long serialVersionUID = 1L;

  public static Builder builder() {
    return new AutoValue_VirusDistributionInfo.Builder();
  }

  public abstract ArrayList<CasesDistributionSet> cases();

  public abstract ArrayList<HospitalisationDistributionSet> hospitalisations();

  public abstract ArrayList<FatalityDistributionSet> fatalities();

  public abstract ArrayList<MunicipalityInfo> municipalities();

  public abstract ArrayList<ProvinceInfo> provinces();

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder cases(ArrayList<CasesDistributionSet> value);

    public abstract Builder hospitalisations(ArrayList<HospitalisationDistributionSet> value);

    public abstract Builder fatalities(ArrayList<FatalityDistributionSet> value);

    public abstract Builder municipalities(ArrayList<MunicipalityInfo> value);

    public abstract Builder provinces(ArrayList<ProvinceInfo> value);

    public abstract VirusDistributionInfo build();
  }
}
