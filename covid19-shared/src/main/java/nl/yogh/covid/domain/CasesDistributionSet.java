package nl.yogh.covid.domain;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.Nullable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class CasesDistributionSet implements Serializable {
  private static final long serialVersionUID = 1L;

  public static Builder builder() {
    return new AutoValue_CasesDistributionSet.Builder();
  } 

  public abstract String source();
  
  @Nullable
  public abstract Integer total();

  public abstract Long date();

  public abstract ArrayList<VirusInfo> provinceDistribution();

  public abstract ArrayList<VirusInfo> municipalityDistribution();

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder source(String value);

    public abstract Builder total(Integer value);

    public abstract Builder date(Long value);

    public abstract Builder provinceDistribution(ArrayList<VirusInfo> value);

    public abstract Builder municipalityDistribution(ArrayList<VirusInfo> value);

    public abstract CasesDistributionSet build();
  }
}
