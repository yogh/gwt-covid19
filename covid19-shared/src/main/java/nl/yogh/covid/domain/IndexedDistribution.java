package nl.yogh.covid.domain;

public interface IndexedDistribution {
  String code();

  int amount();
}
