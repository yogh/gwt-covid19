package nl.yogh.covid.domain;

import java.io.Serializable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class ProvinceInfo implements Serializable {
  private static final long serialVersionUID = 1L;

  public static Builder builder() {
    return new AutoValue_ProvinceInfo.Builder();
  }

  public abstract String code();

  public abstract String name();

  public abstract Integer population();

  public abstract String geometry();

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder code(String value);

    public abstract Builder name(String value);

    public abstract Builder population(Integer value);

    public abstract Builder geometry(String value);

    public abstract ProvinceInfo build();
  }

}
