package nl.yogh.covid.domain;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.Nullable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class HospitalisationDistributionSet implements Serializable {
  private static final long serialVersionUID = 1L;

  public static Builder builder() {
    return new AutoValue_HospitalisationDistributionSet.Builder();
  }

  public abstract String source();
  
  @Nullable
  public abstract Integer total();

  public abstract Long date();

  public abstract ArrayList<HospitalisationInfo> provinceDistribution();

  public abstract ArrayList<HospitalisationInfo> municipalityDistribution();

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder source(String value);

    public abstract Builder total(Integer value);

    public abstract Builder date(Long value);

    public abstract Builder provinceDistribution(ArrayList<HospitalisationInfo> value);

    public abstract Builder municipalityDistribution(ArrayList<HospitalisationInfo> value);

    public abstract HospitalisationDistributionSet build();
  }
}
