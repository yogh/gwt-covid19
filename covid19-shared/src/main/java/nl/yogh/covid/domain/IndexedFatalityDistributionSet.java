package nl.yogh.covid.domain;

import java.util.ArrayList;

import javax.annotation.Nullable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class IndexedFatalityDistributionSet implements IndexedDistributionSet {
  public static Builder builder() {
    return new AutoValue_IndexedFatalityDistributionSet.Builder();
  }

  public abstract String source();

  @Nullable
  public abstract Integer total();

  public abstract Double growth();

  public abstract Long date();

  @Override
  public abstract ArrayList<GrowthInfo> municipalGrowthDistribution();

  @Override
  @Nullable
  public abstract ArrayList<ActiveInfo> municipalActiveDistribution();

  @Override
  public abstract ArrayList<FatalityInfo> municipalityDistribution();

  public abstract ArrayList<FatalityInfo> provinceDistribution();

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder source(String value);

    public abstract Builder total(Integer value);

    public abstract Builder growth(Double value);

    public abstract Builder date(Long value);

    public abstract Builder municipalGrowthDistribution(ArrayList<GrowthInfo> value);

    public abstract Builder municipalActiveDistribution(ArrayList<ActiveInfo> value);

    public abstract Builder municipalityDistribution(ArrayList<FatalityInfo> value);

    public abstract Builder provinceDistribution(ArrayList<FatalityInfo> value);

    public abstract IndexedFatalityDistributionSet build();
  }
}
