package nl.yogh.covid.domain;

import java.util.ArrayList;

import javax.annotation.Nullable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class IndexedCasesDistributionSet implements IndexedDistributionSet {
  public static Builder builder() {
    return new AutoValue_IndexedCasesDistributionSet.Builder();
  }

  public abstract String source();

  @Nullable
  public abstract Integer total();

  @Nullable
  public abstract Integer active();

  @Nullable
  public abstract Double growth();

  public abstract Long date();

  @Override
  public abstract ArrayList<GrowthInfo> municipalGrowthDistribution();

  @Nullable
  @Override
  public abstract ArrayList<ActiveInfo> municipalActiveDistribution();

  @Override
  public abstract ArrayList<VirusInfo> municipalityDistribution();

  public abstract ArrayList<VirusInfo> provinceDistribution();

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder source(String value);

    public abstract Builder total(Integer value);

    public abstract Builder active(Integer value);

    public abstract Builder growth(Double value);

    public abstract Builder date(Long value);

    public abstract Builder municipalGrowthDistribution(ArrayList<GrowthInfo> value);

    public abstract Builder municipalActiveDistribution(ArrayList<ActiveInfo> value);

    public abstract Builder municipalityDistribution(ArrayList<VirusInfo> value);

    public abstract Builder provinceDistribution(ArrayList<VirusInfo> value);

    public abstract IndexedCasesDistributionSet build();
  }
}
