package nl.yogh.covid.domain;

import java.io.Serializable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class ActiveInfo implements Serializable, IndexedDistribution {
  private static final long serialVersionUID = 1L;

  public static Builder builder() {
    return new AutoValue_ActiveInfo.Builder();
  }

  @Override
  public abstract String code();

  @Override
  public abstract int amount();

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder code(String value);

    public abstract Builder amount(int value);

    public abstract ActiveInfo build();
  }
}
