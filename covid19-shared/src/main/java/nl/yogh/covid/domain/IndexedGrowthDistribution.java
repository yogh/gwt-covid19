package nl.yogh.covid.domain;

public interface IndexedGrowthDistribution {
  String code();
  
  Double factor();

  Integer amount();
}
