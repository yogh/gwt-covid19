package nl.yogh.covid.domain;

import java.util.Collection;

public interface IndexedDistributionSet {

  Collection<? extends IndexedDistribution> municipalityDistribution();

  Collection<? extends IndexedGrowthDistribution> municipalGrowthDistribution();

  Collection<? extends IndexedDistribution> municipalActiveDistribution();

  default Collection<? extends IndexedGrowthDistribution> provincialGrowthDistribution() {
    return null;
  }
}
