package nl.yogh.covid.domain;

import java.util.ArrayList;

import javax.annotation.Nullable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class IndexedHospitalisationDistributionSet implements IndexedDistributionSet {
  public static Builder builder() {
    return new AutoValue_IndexedHospitalisationDistributionSet.Builder();
  }

  public abstract String source();

  @Nullable
  public abstract Integer active();

  @Nullable
  public abstract Integer total();

  public abstract Double growth();

  public abstract Long date();

  @Override
  public abstract ArrayList<GrowthInfo> municipalGrowthDistribution();

  @Override
  public abstract ArrayList<ActiveInfo> municipalActiveDistribution();

  @Override
  public abstract ArrayList<HospitalisationInfo> municipalityDistribution();

  public abstract ArrayList<HospitalisationInfo> provinceDistribution();

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder source(String value);

    public abstract Builder active(Integer value);

    public abstract Builder total(Integer value);

    public abstract Builder growth(Double value);

    public abstract Builder date(Long value);

    public abstract Builder municipalGrowthDistribution(ArrayList<GrowthInfo> value);

    public abstract Builder municipalActiveDistribution(ArrayList<ActiveInfo> activeDistribution);

    public abstract Builder municipalityDistribution(ArrayList<HospitalisationInfo> value);

    public abstract Builder provinceDistribution(ArrayList<HospitalisationInfo> value);

    public abstract IndexedHospitalisationDistributionSet build();
  }
}
