# Configuration

## Dataset actualisation

The only configuration needed currently is to put infection-case-number datasets in `/docker/covid-services/cases`.

These files can be manually downloaded from https://www.rivm.nl/coronavirus-kaart-van-nederland-per-gemeente for the current day.

Since there's (historically) been multiple (4) different formats of these flat CSVs, the naming of the cases matters - the web application will use the text preceding a `_` as the format type.

The current format type (as of 2020-03-20) can be denoted with `nieuw-v2`, the second part of the file should be of the form `coronaDDMMYYYY.csv`.

Example for the 2020-03-20 dataset file: `nieuw-v2_corona20032020.csv`.

At least 1 of these files is needed for the application to work without error.

## Installing dependencies

Currently this project depends on another project not yet published in Maven. For now, clone `git@gitlab.com:yogh/gwt-client-common.git` and `mvn install` it.

(See also https://gitlab.com/yogh/gwt-client-common)

# Running in docker

Finish the configuration (i.e. adding a dataset, and install dependencies), and run `deploy.sh` in `/docker`.

# Developing

Import the project (and optionally its dependencies) in Eclipse.

Install the `vue-gwt` plugin.

Install the `m2e-apt` plugin.

Run `serve.sh` in the root directory.
