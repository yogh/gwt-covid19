package nl.yogh.gwt.wui.covid.command;

import io.yogh.gwt.wui.command.SimpleGenericCommand;

import nl.yogh.covid.domain.IndexedFatalityDistributionSet;
import nl.yogh.gwt.wui.covid.event.FatalityDistributionSetChangeEvent;

public class FatalityDistributionSetChangeCommand
    extends SimpleGenericCommand<IndexedFatalityDistributionSet, FatalityDistributionSetChangeEvent> {
  public FatalityDistributionSetChangeCommand(final IndexedFatalityDistributionSet value) {
    super(value);
  }

  @Override
  protected FatalityDistributionSetChangeEvent createEvent(final IndexedFatalityDistributionSet value) {
    return new FatalityDistributionSetChangeEvent(value);
  }
}
