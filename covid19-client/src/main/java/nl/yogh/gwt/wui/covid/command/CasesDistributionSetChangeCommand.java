package nl.yogh.gwt.wui.covid.command;

import io.yogh.gwt.wui.command.SimpleGenericCommand;

import nl.yogh.covid.domain.IndexedCasesDistributionSet;
import nl.yogh.gwt.wui.covid.event.CasesDistributionSetChangeEvent;

public class CasesDistributionSetChangeCommand extends SimpleGenericCommand<IndexedCasesDistributionSet, CasesDistributionSetChangeEvent> {
  public CasesDistributionSetChangeCommand(final IndexedCasesDistributionSet value) {
    super(value);
  }

  @Override
  protected CasesDistributionSetChangeEvent createEvent(final IndexedCasesDistributionSet value) {
    return new CasesDistributionSetChangeEvent(value);
  }
}
