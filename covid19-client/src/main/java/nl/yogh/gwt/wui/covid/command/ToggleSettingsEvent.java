package nl.yogh.gwt.wui.covid.command;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

public class ToggleSettingsEvent extends SimpleGenericEvent<Boolean> {
  public ToggleSettingsEvent(final boolean value) {
    super(value);
  }
}
