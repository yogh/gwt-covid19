package nl.yogh.gwt.wui.bootstrap;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import io.yogh.gwt.vue.component.BasicVueComponent;

@Component
public class BootstrapLoadingView extends BasicVueComponent implements IsVueComponent {}
