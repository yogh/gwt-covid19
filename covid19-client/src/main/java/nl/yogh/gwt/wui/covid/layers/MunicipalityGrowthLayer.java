package nl.yogh.gwt.wui.covid.layers;

import com.google.web.bindery.event.shared.EventBus;

import ol.Map;
import ol.OLFactory;
import ol.proj.Projection;
import ol.style.Style;
import ol.style.StyleOptions;

import nl.yogh.covid.domain.GrowthInfo;
import nl.yogh.covid.domain.MunicipalityInfo;

public class MunicipalityGrowthLayer extends MunicipalityLayer<GrowthInfo> {
  public MunicipalityGrowthLayer(final Map map, final Projection projection, final EventBus eventBus) {
    super(map, projection, eventBus);
  }

  @Override
  protected Style createStyle(final MunicipalityInfo area, final GrowthInfo info) {
    final StyleOptions styleOptions = new StyleOptions();
    styleOptions.setStroke(OLFactory.createStroke(OLFactory.createColor(214, 51, 39, 0.1), 2));

    if (info != null) {
      if (info.factor() == null) {
        styleOptions.setFill(OLFactory.createFill(OLFactory.createColor(255, 255, 255, 0.1)));
      } else {
        if (info.factor() == 1D) {
          styleOptions.setFill(OLFactory.createFill(OLFactory.createColor(255, 255, 255, 0.3)));
        } else if (info.factor() > 1D) {
          styleOptions.setFill(OLFactory.createFill(OLFactory.createColor(255, 0, 0, info.factor() - 1)));
        } else {
          styleOptions.setFill(OLFactory.createFill(OLFactory.createColor(0, 255, 0, 1 - info.factor())));
        }
        
      }
    }

    return new Style(styleOptions);
  }
}
