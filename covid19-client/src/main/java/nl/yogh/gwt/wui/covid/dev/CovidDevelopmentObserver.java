package nl.yogh.gwt.wui.covid.dev;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;
import com.google.web.bindery.event.shared.binder.GenericEvent;

import io.yogh.gwt.wui.command.PlaceChangeCommand;
import io.yogh.gwt.wui.command.PlaceChangeRequestCommand;
import io.yogh.gwt.wui.command.SimpleGenericCommand;
import io.yogh.gwt.wui.dev.DevelopmentObserver;
import io.yogh.gwt.wui.dev.GWTProd;
import io.yogh.gwt.wui.event.BasicEventComponent;
import io.yogh.gwt.wui.event.PlaceChangeEvent;
import io.yogh.gwt.wui.event.SimpleGenericEvent;
import io.yogh.gwt.wui.main.event.NotificationEvent;

@Singleton
public class CovidDevelopmentObserver extends BasicEventComponent implements DevelopmentObserver {
  interface DevelopmentObserverMonitorImplEventBinder extends EventBinder<CovidDevelopmentObserver> {}

  private final DevelopmentObserverMonitorImplEventBinder EVENT_BINDER = GWT.create(DevelopmentObserverMonitorImplEventBinder.class);

  @Inject
  public CovidDevelopmentObserver() {}

  @EventHandler(handles = { PlaceChangeCommand.class })
  public void onSimpleGenericCommand(@SuppressWarnings("rawtypes") final SimpleGenericCommand e) {
    log(e.getClass().getSimpleName(), e.getValue());
  }

  // @EventHandler(handles = {})
  public void onSimpleGenericCommand(final GenericEvent e) {
    log(e.getClass().getSimpleName());
  }

  @EventHandler(handles = { NotificationEvent.class, PlaceChangeEvent.class, PlaceChangeRequestCommand.class })
  public void onSimpleGenericEvent(@SuppressWarnings("rawtypes") final SimpleGenericEvent e) {
    log(e.getClass().getSimpleName(), e.getValue());
    brbr();
  }

  private void brbr() {
    logRaw("");
  }

  private void log(final String origin) {
    logRaw("[" + origin + "]");
  }

  private void log(final String origin, final Object val) {
    logRaw("[" + origin + "] " + String.valueOf(val));
  }

  private void logRaw(final String string) {
    GWTProd.log(string);
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
