package nl.yogh.gwt.wui.covid.context;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import com.axellience.vuegwt.core.annotations.component.Data;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.yogh.covid.domain.IndexedCasesDistributionSet;
import nl.yogh.covid.domain.IndexedDistributionSet;
import nl.yogh.covid.domain.IndexedFatalityDistributionSet;
import nl.yogh.covid.domain.IndexedHospitalisationDistributionSet;

@Singleton
public class VirusContext {
  public enum DataType {
    CUMULATIVE_CASES("positief-cumulatief"),
    ACTIVE_CASES("positief-actief"),
    CUMULATIVE_HOSPITALISATIONS("opnames-cumulatief"),
    ACTIVE_HOSPITALISATIONS("opnames-actief"),
    CUMULATIVE_FATALITIES("overleden-cumulatief");

    private final String title;

    DataType(final String title) {
      this.title = title;
    }

    public static DataType fromTitle(final String title) {
      for (final DataType panelName : values()) {
        if (panelName.getTitle().equals(title)) {
          return panelName;
        }
      }

      return null;
    }

    public static DataType[] activeValues() {
      return new DataType[] { CUMULATIVE_CASES, ACTIVE_CASES, CUMULATIVE_HOSPITALISATIONS, CUMULATIVE_FATALITIES };
    }

    public String getTitle() {
      return title;
    }
  }

  @JsProperty @Data List<IndexedCasesDistributionSet> cases = new ArrayList<>();
  @JsProperty @Data List<IndexedHospitalisationDistributionSet> hospitalisations = new ArrayList<>();
  @JsProperty @Data List<IndexedFatalityDistributionSet> fatalities = new ArrayList<>();

  @Data DataType active;

  public List<IndexedCasesDistributionSet> getCasesDistributionData() {
    return cases;
  }

  public IndexedCasesDistributionSet getCasesDistributionData(final String set) {
    return cases.stream()
        .filter(v -> v.source().equals(set))
        .findAny().orElse(null);
  }

  public DataType getActive() {
    return active;
  }

  public void setActive(final DataType type) {
    this.active = type;
  }

  public void setCasesDistributionData(final List<IndexedCasesDistributionSet> cases) {
    this.cases = cases;
  }

  public void setHospitalisationDistributionData(final List<IndexedHospitalisationDistributionSet> hospitalisations) {
    this.hospitalisations = hospitalisations;
  }

  public void setFatalitiesDistributionData(final List<IndexedFatalityDistributionSet> fatalities) {
    this.fatalities = fatalities;
  }

  public List<IndexedHospitalisationDistributionSet> getHospitalisationDistributionData() {
    return hospitalisations;
  }

  public IndexedHospitalisationDistributionSet getHospitalisationsDistributionData(final String set) {
    return hospitalisations.stream()
        .filter(v -> v.source().equals(set))
        .findAny().orElse(null);
  }

  public List<IndexedHospitalisationDistributionSet> getHospitalisationDistributionDatas() {
    return hospitalisations;
  }

  public IndexedFatalityDistributionSet getFatalityDistributionData(final String set) {
    return fatalities.stream()
        .filter(v -> v.source().equals(set))
        .findAny().orElse(null);
  }

  public List<IndexedFatalityDistributionSet> getFatalityDistributionData() {
    return fatalities;
  }

  public List<? extends IndexedDistributionSet> getActiveDistributionData() {
    return active == DataType.CUMULATIVE_HOSPITALISATIONS || active == DataType.ACTIVE_HOSPITALISATIONS ? hospitalisations
        : active == DataType.CUMULATIVE_CASES || active == DataType.ACTIVE_CASES ? cases
            : fatalities;
  }

  @JsMethod
  public boolean isActive(final DataType type) {
    return active == type;
  }
}
