package nl.yogh.gwt.wui.covid.place;

import java.util.List;
import java.util.function.Supplier;

import io.yogh.gwt.wui.place.SequenceTokenizer;

import nl.yogh.gwt.wui.covid.context.VirusContext.DataType;
import nl.yogh.gwt.wui.covid.place.CovidPlaces.MapPlace;

public class MapTokenizer<M extends MapPlace> extends SequenceTokenizer<M> {
  private final Supplier<M> supplier;
  private final String prefix;

  public MapTokenizer(final Supplier<M> supplier, final String prefix) {
    this.supplier = supplier;
    this.prefix = prefix;
  }

  public static <M extends MapPlace> MapTokenizer<M> createMap(final Supplier<M> supplier, final String prefix) {
    return new MapTokenizer<M>(supplier, prefix);
  }

  @Override
  protected void updatePlace(final List<String> tokens, final M place) {
    if (tokens.size() > 1) {
      place.setType(DataType.fromTitle(tokens.get(1)));
    }
  }

  @Override
  protected void setTokenList(final M place, final List<String> tokens) {
    if (place.getType() != null && place.getType() != MapPlace.DEFAULT_DATA_TYPE) {
      tokens.add(place.getType().getTitle());
    }
  }

  @Override
  public String getPrefix() {
    return prefix;
  }

  @Override
  protected M createPlace() {
    return supplier.get();
  }
}
