package nl.yogh.gwt.wui.covid.ui.map;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;

import nl.yogh.gwt.wui.covid.ui.MainView;

public class MapActivity implements MapPresenter {
  @Inject
  public MapActivity(@Assisted final MainView view) {
    view.setMain(MapViewFactory.get());
  }

  @Override
  public void setView(final MapView view) {}
}
