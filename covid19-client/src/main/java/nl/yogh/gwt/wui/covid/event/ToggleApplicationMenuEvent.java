package nl.yogh.gwt.wui.covid.event;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

public class ToggleApplicationMenuEvent extends SimpleGenericEvent<Boolean> {
  public ToggleApplicationMenuEvent(final boolean open) {
    super(open);
  }
}
