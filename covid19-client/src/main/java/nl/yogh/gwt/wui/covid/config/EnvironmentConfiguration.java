package nl.yogh.gwt.wui.covid.config;

import com.google.inject.ImplementedBy;

@ImplementedBy(EnvironmentConfigurationImpl.class)
public interface EnvironmentConfiguration {
  String getApiHost();
}
