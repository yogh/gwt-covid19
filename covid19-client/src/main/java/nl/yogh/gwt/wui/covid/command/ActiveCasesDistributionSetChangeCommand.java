package nl.yogh.gwt.wui.covid.command;

import io.yogh.gwt.wui.command.SimpleGenericCommand;

import nl.yogh.covid.domain.IndexedCasesDistributionSet;
import nl.yogh.gwt.wui.covid.event.ActiveCasesDistributionSetChangeEvent;

public class ActiveCasesDistributionSetChangeCommand
    extends SimpleGenericCommand<IndexedCasesDistributionSet, ActiveCasesDistributionSetChangeEvent> {
  public ActiveCasesDistributionSetChangeCommand(final IndexedCasesDistributionSet value) {
    super(value);
  }

  @Override
  protected ActiveCasesDistributionSetChangeEvent createEvent(final IndexedCasesDistributionSet value) {
    return new ActiveCasesDistributionSetChangeEvent(value);
  }
}
