package nl.yogh.gwt.wui.covid.mapper;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Stream;

import io.yogh.gwt.wui.dev.GWTProd;
import io.yogh.gwt.wui.history.PlaceHistoryMapper;
import io.yogh.gwt.wui.place.PlaceTokenizer;
import io.yogh.gwt.wui.place.TokenizedPlace;

import nl.yogh.gwt.wui.covid.place.CovidTokenizers;

public class CovidPlaceHistoryMapper implements PlaceHistoryMapper {
  private final Map<String, PlaceTokenizer<? extends TokenizedPlace>> tokenizers = new LinkedHashMap<>();

  public CovidPlaceHistoryMapper() {
    init(CovidTokenizers.TOKENIZERS);
  }

  private void init(final PlaceTokenizer<?>... tokenizers) {
    Stream.of(tokenizers).forEach(this::init);
  }

  private void init(final PlaceTokenizer<?> tokenizer) {
    tokenizers.put(tokenizer.getPrefix(), tokenizer);
  }

  @Override
  public String getToken(final TokenizedPlace value) {
    GWTProd.log("Getting token of: " + value);
    
    GWTProd.log(value.getTokenizer());

    GWTProd.log("Tokenizer: " + " > " + value.getToken());
    final String token = value.getToken();

    GWTProd.log("Result: " + token);
    return token;
  }

  @Override
  public TokenizedPlace getPlace(final String fullToken) {
    GWTProd.log("Full token: " + fullToken);

    final String trimmedToken = trimToken(fullToken);

    GWTProd.log("Trimmed token: " + trimmedToken);

    return tokenizers.keySet().stream()
        .filter(v -> trimmedToken.startsWith(v))
        .findFirst()
        .map(v -> tokenizers.get(v).getPlace(fullToken.substring(v.length())))
        .orElse(null);
  }

  private String trimToken(final String fullToken) {
    return trimEnd(trimStart(fullToken));
  }

  private String trimEnd(final String token) {
    return token.lastIndexOf("/") == token.length() - 1 ? trimEnd(token.substring(0, token.length() - 1)) : token;
  }

  private String trimStart(final String token) {
    return token.indexOf("/") == 0 ? trimStart(token.substring(1)) : token;
  }
}
