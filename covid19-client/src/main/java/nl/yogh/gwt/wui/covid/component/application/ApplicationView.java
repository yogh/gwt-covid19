package nl.yogh.gwt.wui.covid.component.application;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.web.bindery.event.shared.EventBus;

import nl.yogh.gwt.wui.component.CovidVueComponent;

@Component(components = {
    ApplicationHeader.class,
    ApplicationNavigation.class
})
public class ApplicationView extends CovidVueComponent implements IsVueComponent {
  @Prop EventBus eventBus;
}
