package nl.yogh.gwt.wui.covid.daemon;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

public class VirusDistributionLoadError extends SimpleGenericEvent<String> {
  public VirusDistributionLoadError(final String msg) {
    super(msg);
  }
}
