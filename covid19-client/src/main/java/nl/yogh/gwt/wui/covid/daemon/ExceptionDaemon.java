package nl.yogh.gwt.wui.covid.daemon;

import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.wui.dev.GWTProd;
import io.yogh.gwt.wui.event.BasicEventComponent;
import io.yogh.gwt.wui.event.RequestClientLoadFailureEvent;
import io.yogh.gwt.wui.event.RequestConnectionFailureEvent;
import io.yogh.gwt.wui.event.RequestServerLoadFailureEvent;
import io.yogh.gwt.wui.init.ExceptionalPirateCache;
import io.yogh.gwt.wui.main.util.NotificationUtil;
import io.yogh.gwt.wui.service.exception.RequestBlockedException;
import io.yogh.gwt.wui.service.exception.RequestClientException;
import io.yogh.gwt.wui.service.exception.RequestServerException;
import io.yogh.gwt.wui.widget.HasEventBus;

public class ExceptionDaemon extends BasicEventComponent implements HasEventBus {
  private Throwable findCause(final Throwable e) {
    final Throwable cause;

    final Throwable booty = ExceptionalPirateCache.pop();
    if (booty == null) {
      if (e == null) {
        cause = null;
      } else if (e.getCause() == null) {
        cause = e;
      } else {
        // Recurse down
        cause = findCause(e.getCause());
      }
    } else {
      // Recurse on the hidden exception
      cause = findCause(booty);
    }

    return cause;
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus);

    GWTProd.log("Setting exception handler in Daemon.");
    GWT.setUncaughtExceptionHandler(e -> {
      final Throwable cause = findCause(e);

      if (cause instanceof RequestClientException) {
        eventBus.fireEvent(new RequestClientLoadFailureEvent(true));
      } else if (cause instanceof RequestServerException) {
        eventBus.fireEvent(new RequestServerLoadFailureEvent(true));
      } else if (cause instanceof RequestBlockedException) {
        eventBus.fireEvent(new RequestConnectionFailureEvent(true));
      } else {
        GWTProd.error(cause);
        NotificationUtil.broadcastError(eventBus, cause);
      }
    });
  }
}
