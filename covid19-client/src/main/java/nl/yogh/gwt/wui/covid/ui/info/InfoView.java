package nl.yogh.gwt.wui.covid.ui.info;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasActivated;
import com.axellience.vuegwt.core.client.component.hooks.HasDeactivated;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;

import io.yogh.gwt.geo.wui.OL3MapComponent;

import nl.yogh.gwt.wui.component.CovidVueComponent;

@Component(components = {
    OL3MapComponent.class
})
public class InfoView extends CovidVueComponent implements IsVueComponent, HasActivated, HasDeactivated {
  private static final GraphViewEventBinder EVENT_BINDER = GWT.create(GraphViewEventBinder.class);

  interface GraphViewEventBinder extends EventBinder<InfoView> {}

  @Prop EventBus eventBus;

  private HandlerRegistration bindEventHandlers;

  @Override
  public void deactivated() {
    bindEventHandlers.removeHandler();
  }

  @Override
  public void activated() {
    bindEventHandlers = EVENT_BINDER.bindEventHandlers(this, eventBus);
  }
}
