package nl.yogh.gwt.wui.covid.event;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

import nl.yogh.gwt.wui.covid.context.VirusContext.DataType;

public class VirusDataTypeChangedCommand extends SimpleGenericEvent<DataType> {
  public VirusDataTypeChangedCommand(final DataType value) {
    super(value);
  }
}
