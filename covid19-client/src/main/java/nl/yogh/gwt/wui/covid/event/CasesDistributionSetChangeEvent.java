package nl.yogh.gwt.wui.covid.event;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

import nl.yogh.covid.domain.IndexedCasesDistributionSet;

public class CasesDistributionSetChangeEvent extends SimpleGenericEvent<IndexedCasesDistributionSet> {
  public CasesDistributionSetChangeEvent(final IndexedCasesDistributionSet value) {
    super(value);
  }
}
