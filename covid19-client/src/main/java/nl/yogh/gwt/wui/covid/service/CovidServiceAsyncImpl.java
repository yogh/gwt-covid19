/*
 * Copyright Dutch Ministry of Economic Affairs
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.yogh.gwt.wui.covid.service;

import javax.inject.Inject;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Singleton;

import io.yogh.gwt.wui.util.RequestUtil;
import io.yogh.gwt.wui.util.TextUtil;

import nl.yogh.covid.domain.RequestMapping;
import nl.yogh.covid.domain.VirusDistributionInfo;
import nl.yogh.covid.domain.VirusDistributionInfoJsonParser;
import nl.yogh.gwt.wui.covid.config.EnvironmentConfiguration;

@Singleton
public class CovidServiceAsyncImpl implements CovidServiceAsync {
  @Inject EnvironmentConfiguration cfg;

  @Override
  public void getViralDistribution(final AsyncCallback<VirusDistributionInfo> callback) {
    final String url = TextUtil.formatString(cfg.getApiHost() + RequestMapping.GET_DISTRIBUTION);
    RequestUtil.doGet(url, v -> VirusDistributionInfoJsonParser.wrap(v), callback);
  }

  @Override
  public void getViralDistributionLimited(final AsyncCallback<VirusDistributionInfo> callback) {
    final String url = TextUtil.formatString(cfg.getApiHost() + RequestMapping.GET_DISTRIBUTION_LIMIT);
    RequestUtil.doGet(url, v -> VirusDistributionInfoJsonParser.wrap(v), callback);
  }
}
