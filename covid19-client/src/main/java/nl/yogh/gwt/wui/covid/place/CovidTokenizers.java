/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.yogh.gwt.wui.covid.place;

import io.yogh.gwt.wui.place.ApplicationPlace.Tokenizer;
import io.yogh.gwt.wui.place.PlaceTokenizer;

import nl.yogh.gwt.wui.covid.place.CovidPlaces.GraphPlace;
import nl.yogh.gwt.wui.covid.place.CovidPlaces.InfoPlace;
import nl.yogh.gwt.wui.covid.place.CovidPlaces.MapPlace;

public class CovidTokenizers {
  private static final String ALIAS_MAP = "map";
  private static final String ALIAS_GRAPH = "grafiek";
  private static final String ALIAS_INFO = "info";

  public static final PlaceTokenizer<MapPlace> MAP = MapTokenizer.createMap(() -> new MapPlace(), ALIAS_MAP);
  public static final PlaceTokenizer<GraphPlace> GRAPH = Tokenizer.create(() -> new GraphPlace(), ALIAS_GRAPH);
  public static final PlaceTokenizer<InfoPlace> INFO = Tokenizer.create(() -> new InfoPlace(), ALIAS_INFO);

  public static final PlaceTokenizer<?>[] TOKENIZERS = new PlaceTokenizer[] {
      MAP, GRAPH, INFO
  };
}
