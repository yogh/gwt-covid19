package nl.yogh.gwt.wui.covid.ui;

import javax.inject.Inject;

import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.vue.activity.AbstractVueActivity;
import io.yogh.gwt.wui.activity.DelegableActivity;
import io.yogh.gwt.wui.command.PlaceChangeCommand;
import io.yogh.gwt.wui.place.Place;

import nl.yogh.gwt.wui.covid.place.CovidPlaces.MainPlace;

public class MainActivity extends AbstractVueActivity<MainPresenter, MainView, MainViewFactory> implements MainPresenter, DelegableActivity {
  private final MainActivityManager delegator;

  @Inject
  public MainActivity(final MainActivityManager delegator) {
    super(MainViewFactory.get());
    this.delegator = delegator;
  }

  @Override
  public MainActivity getPresenter() {
    return this;
  }

  @Override
  public boolean delegate(final EventBus eventBus, final PlaceChangeCommand c) {
    final boolean delegated = delegator.delegate(eventBus, c.getValue(), c::setRedirect);

    return delegated;
  }

  @Override
  public void setView(final MainView view) {
    delegator.setView(view);
  }

  @Override
  public void onStart() {

  }

  @Override
  public boolean isDelegable(final Place place) {
    return place instanceof MainPlace;
  }
}
