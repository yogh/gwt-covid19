package nl.yogh.gwt.wui.covid.ui.info;

import nl.yogh.gwt.wui.covid.ui.MainSubPresenter;

public interface InfoPresenter extends MainSubPresenter {
  void setView(InfoView view);
}
