package nl.yogh.gwt.wui.covid.daemon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.EvictingQueue;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import io.yogh.gwt.geo.command.LayerAddedCommand;
import io.yogh.gwt.geo.command.LayerRemovedCommand;
import io.yogh.gwt.geo.domain.IsLayer;
import io.yogh.gwt.geo.domain.IsMapCohort;
import io.yogh.gwt.geo.domain.LayerInfo;
import io.yogh.gwt.geo.epsg.EPSG;
import io.yogh.gwt.geo.event.MapEventBus;
import io.yogh.gwt.geo.wui.Map;
import io.yogh.gwt.geo.wui.util.OL3MapUtil;
import io.yogh.gwt.wui.dev.GWTProd;
import io.yogh.gwt.wui.event.BasicEventComponent;
import io.yogh.gwt.wui.future.AppAsyncCallback;
import io.yogh.gwt.wui.place.Place;
import io.yogh.gwt.wui.place.PlaceController;

import ol.OLFactory;
import ol.layer.Layer;
import ol.layer.LayerOptions;
import ol.layer.Tile;
import ol.proj.Projection;
import ol.source.Wmts;
import ol.source.WmtsOptions;

import nl.yogh.covid.domain.ActiveInfo;
import nl.yogh.covid.domain.CasesDistributionSet;
import nl.yogh.covid.domain.FatalityDistributionSet;
import nl.yogh.covid.domain.FatalityInfo;
import nl.yogh.covid.domain.GrowthInfo;
import nl.yogh.covid.domain.HospitalisationDistributionSet;
import nl.yogh.covid.domain.HospitalisationInfo;
import nl.yogh.covid.domain.IndexedCasesDistributionSet;
import nl.yogh.covid.domain.IndexedFatalityDistributionSet;
import nl.yogh.covid.domain.IndexedHospitalisationDistributionSet;
import nl.yogh.covid.domain.VirusDistributionInfo;
import nl.yogh.covid.domain.VirusInfo;
import nl.yogh.gwt.wui.covid.command.ActiveCasesDistributionSetChangeCommand;
import nl.yogh.gwt.wui.covid.command.ActiveHospitalisationDistributionSetChangeCommand;
import nl.yogh.gwt.wui.covid.command.CasesDistributionSetChangeCommand;
import nl.yogh.gwt.wui.covid.command.FatalityDistributionSetChangeCommand;
import nl.yogh.gwt.wui.covid.command.HospitalisationDistributionSetChangeCommand;
import nl.yogh.gwt.wui.covid.context.MapContext;
import nl.yogh.gwt.wui.covid.context.VirusContext;
import nl.yogh.gwt.wui.covid.context.VirusContext.DataType;
import nl.yogh.gwt.wui.covid.event.MunicipalityHoverEvent;
import nl.yogh.gwt.wui.covid.event.MunicipalitySelectedEvent;
import nl.yogh.gwt.wui.covid.event.VirusDataTypeChangedCommand;
import nl.yogh.gwt.wui.covid.event.VirusDistributionChangedEvent;
import nl.yogh.gwt.wui.covid.layers.MunicipalityActiveCasesLayer;
import nl.yogh.gwt.wui.covid.layers.MunicipalityActiveHospitalisationLayer;
import nl.yogh.gwt.wui.covid.layers.MunicipalityCasesLayer;
import nl.yogh.gwt.wui.covid.layers.MunicipalityFatalitiesLayer;
import nl.yogh.gwt.wui.covid.layers.MunicipalityHospitalisationLayer;
import nl.yogh.gwt.wui.covid.place.CovidPlaces.MapPlace;
import nl.yogh.gwt.wui.covid.service.CovidServiceAsync;

public class VirusDistributionDaemon extends BasicEventComponent implements IsMapCohort {
  private static final VirusDistributionDaemonEventBinder EVENT_BINDER = GWT.create(VirusDistributionDaemonEventBinder.class);

  interface VirusDistributionDaemonEventBinder extends EventBinder<VirusDistributionDaemon> {}

  public static final DataType DEFAULT_DATATYPE = DataType.CUMULATIVE_HOSPITALISATIONS;

  // Assume 14 days
  private final int CUMULATIVE_EVICTION_DAYS = 2 * 7;

  @Inject CovidServiceAsync service;

  @Inject EPSG epsg;

  @Inject VirusContext context;

  @Inject PlaceController placeController;

  private MapEventBus mapEventBus;
  private ol.Map map;

  private MunicipalityCasesLayer municipalityCasesLayer;
  private MunicipalityActiveCasesLayer municipalityActiveCasesLayer;
  private MunicipalityHospitalisationLayer municipalityHospitalisationLayer;
  private MunicipalityActiveHospitalisationLayer municipalityActiveHospitalisationLayer;
  private MunicipalityFatalitiesLayer municipalityFatalitiesLayer;

  private VirusDistributionInfo distribution;

  private List<IndexedCasesDistributionSet> casesProvisioned;
  private List<IndexedHospitalisationDistributionSet> hospitalisationsProvisioned;
  private List<IndexedFatalityDistributionSet> fatalitiesProvisioned;

  private IsLayer<?> activeLayer;
  private final List<IsLayer<?>> layers = new ArrayList<>();

  @Inject
  public VirusDistributionDaemon(final MapContext mapContext) {
    mapContext.registerPrimaryMapCohort(this);
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }

  @EventHandler
  public void onVirusDistributionChangedEvent(final VirusDistributionChangedEvent e) {
    distribution = e.getValue();

    casesProvisioned = provisionCases(distribution.cases());
    context.setCasesDistributionData(casesProvisioned);

    final Runnable later = () -> {
      hospitalisationsProvisioned = provisionHosp(distribution.hospitalisations());
      fatalitiesProvisioned = provisionFatl(distribution.fatalities());
      context.setHospitalisationDistributionData(hospitalisationsProvisioned);
      context.setFatalitiesDistributionData(fatalitiesProvisioned);
    };

    municipalityCasesLayer.setAreas(distribution.municipalities());
    municipalityActiveCasesLayer.setAreas(distribution.municipalities());
    municipalityHospitalisationLayer.setAreas(distribution.municipalities());
    municipalityActiveHospitalisationLayer.setAreas(distribution.municipalities());
    municipalityFatalitiesLayer.setAreas(distribution.municipalities());

    layers.clear();
    layers.add(municipalityCasesLayer);
    layers.add(municipalityActiveCasesLayer);
    layers.add(municipalityHospitalisationLayer);
    layers.add(municipalityActiveHospitalisationLayer);
    layers.add(municipalityFatalitiesLayer);

    final Place place = placeController.getPlace();
    DataType initialDataType = null;
    if (place instanceof MapPlace) {
      initialDataType = ((MapPlace) place).getType();
    }

    initialDataType = initialDataType == null ? MapPlace.DEFAULT_DATA_TYPE : initialDataType;
    eventBus.fireEvent(new VirusDataTypeChangedCommand(initialDataType));

    Scheduler.get().scheduleDeferred(() -> {
      later.run();
    });
  }

  public void setDataType(final DataType type) {
    if (context.isActive(type)) {
      return;
    }

    context.setActive(type);

    placeController.goTo(new MapPlace(type));

    switch (type) {
    case CUMULATIVE_CASES:
      final IndexedCasesDistributionSet cumCasesSet = casesProvisioned.get(casesProvisioned.size() - 1);
      eventBus.fireEvent(new CasesDistributionSetChangeCommand(cumCasesSet));

      setLayer(municipalityCasesLayer);
      break;
    case ACTIVE_CASES:
      final IndexedCasesDistributionSet actCasesSet = casesProvisioned.get(casesProvisioned.size() - 1);
      eventBus.fireEvent(new ActiveCasesDistributionSetChangeCommand(actCasesSet));

      setLayer(municipalityActiveCasesLayer);
      break;
    case CUMULATIVE_HOSPITALISATIONS:
      final IndexedHospitalisationDistributionSet cumHospitalisationsSet = hospitalisationsProvisioned.get(hospitalisationsProvisioned.size() - 1);
      eventBus.fireEvent(new HospitalisationDistributionSetChangeCommand(cumHospitalisationsSet));

      setLayer(municipalityHospitalisationLayer);
      break;
    case ACTIVE_HOSPITALISATIONS:
      final IndexedHospitalisationDistributionSet actHospitalisationsSet = hospitalisationsProvisioned.get(hospitalisationsProvisioned.size() - 1);
      eventBus.fireEvent(new ActiveHospitalisationDistributionSetChangeCommand(actHospitalisationsSet));

      setLayer(municipalityActiveHospitalisationLayer);
      break;
    case CUMULATIVE_FATALITIES:
      final IndexedFatalityDistributionSet fatalitiesSet = fatalitiesProvisioned.get(fatalitiesProvisioned.size() - 1);
      eventBus.fireEvent(new FatalityDistributionSetChangeCommand(fatalitiesSet));

      setLayer(municipalityFatalitiesLayer);
      break;
    }
  }

  private void setLayer(final IsLayer<Layer> layer) {
    if (activeLayer == layer) {
      return;
    }

    final List<IsLayer<?>> layerz = new ArrayList<>(layers);
    layerz.remove(layer);

    layerz.forEach(v -> mapEventBus.fireEvent(new LayerRemovedCommand(v)));
    layerz.forEach(v -> GWTProd.log("Removed: " + v));
    layer.asLayer().setVisible(true);
    mapEventBus.fireEvent(new LayerAddedCommand(layer));

    activeLayer = layer;
  }

  public static IsLayer<Layer> prepareLabels(final Projection projection, final EPSG epsg) {
    final WmtsOptions wmtsOptions = OLFactory.createOptions();
    // https://geodata.nationaalgeoregister.nl/tiles/service/wmts?request=GetCapabilities&service=WMTS
    wmtsOptions.setUrl("https://geodata.nationaalgeoregister.nl/tiles/service/wmts");
    wmtsOptions.setLayer("lufolabels");
    wmtsOptions.setFormat("image/png8");
    wmtsOptions.setMatrixSet(epsg.getEpsgCode());
    wmtsOptions.setStyle("default");
    wmtsOptions.setProjection(projection);
    wmtsOptions.setWrapX(true);
    wmtsOptions.setTileGrid(OL3MapUtil.createWmtsTileGrid(projection));

    final Wmts wmtsSource = new Wmts(wmtsOptions);

    final LayerOptions wmtsLayerOptions = OLFactory.createOptions();
    wmtsLayerOptions.setSource(wmtsSource);

    final Tile wmtsLayer = new Tile(wmtsLayerOptions);
    wmtsLayer.setOpacity(1);
    wmtsLayer.setZIndex(9910005);
    wmtsLayer.setVisible(true);

    final LayerInfo info = new LayerInfo();
    info.setTitle("Luchtfoto labels");
    info.setName("3");

    return OL3MapUtil.wrap(wmtsLayer, info);
  }

  private Double calculateGrowth(final int current, final int previous) {
    return previous == 0 ? 2 : (double) current / (double) previous;
  }

  private List<IndexedFatalityDistributionSet> provisionFatl(final List<FatalityDistributionSet> data) {
    final List<IndexedFatalityDistributionSet.Builder> lst = new ArrayList<>(data.size());
    data.forEach(item -> {
      lst.add(IndexedFatalityDistributionSet.builder()
          .date(item.date())
          .total(item.total())
          .source(item.source())
          .municipalityDistribution(item.municipalityDistribution())
          .provinceDistribution(item.provinceDistribution()));
    });

    // Horrible
    java.util.Map<String, FatalityInfo> previousDistribution = new HashMap<>();
    for (int i = 0; i < data.size(); i++) {
      final FatalityDistributionSet current = data.get(i);

      final IndexedFatalityDistributionSet.Builder target = lst.get(i);

      final ArrayList<GrowthInfo> growthDistribution = new ArrayList<>();
      for (int j = 0; j < current.municipalityDistribution().size(); j++) {
        final FatalityInfo currentVirusInfo = current.municipalityDistribution().get(j);
        final FatalityInfo previousVirusInfo = previousDistribution.get(currentVirusInfo.code());

        final GrowthInfo.Builder bldr = GrowthInfo.builder()
            .code(currentVirusInfo.code());

        if (previousVirusInfo == null || previousVirusInfo.amount() == 0) {
          bldr.amount(currentVirusInfo.amount());
          bldr.factor(null);
        } else {
          bldr.amount(currentVirusInfo.amount() - previousVirusInfo.amount());
          bldr.factor((double) currentVirusInfo.amount() / (double) previousVirusInfo.amount());
        }

        growthDistribution.add(bldr.build());
      }

      target.growth(calculateGrowth(
          current.municipalityDistribution().stream()
              .mapToInt(v -> v.amount()).sum(),
          previousDistribution.values().stream()
              .mapToInt(v -> v.amount()).sum()));

      target.municipalGrowthDistribution(growthDistribution);
      previousDistribution = current.municipalityDistribution().stream()
          .collect(Collectors.toMap(v -> v.code(), v -> v));
    }

    return lst.stream()
        .map(v -> v.build())
        .collect(Collectors.toList());
  }

  private List<IndexedHospitalisationDistributionSet> provisionHosp(final List<HospitalisationDistributionSet> data) {
    final List<IndexedHospitalisationDistributionSet.Builder> lst = new ArrayList<>(data.size());
    data.forEach(item -> {
      lst.add(IndexedHospitalisationDistributionSet.builder()
          .date(item.date())
          .total(item.total())
          .source(item.source())
          .municipalityDistribution(item.municipalityDistribution())
          .provinceDistribution(item.provinceDistribution()));
    });

    // Horrible
    final java.util.Map<String, EvictingQueue<Integer>> activeDistributionTracker = new HashMap<>();
    java.util.Map<String, HospitalisationInfo> previousDistribution = new HashMap<>();
    final EvictingQueue<Integer> totalActiveTracker = EvictingQueue.create(CUMULATIVE_EVICTION_DAYS);
    for (int i = 0; i < data.size(); i++) {
      final HospitalisationDistributionSet current = data.get(i);

      final IndexedHospitalisationDistributionSet.Builder target = lst.get(i);

      int setActiveAmount = 0;
      final ArrayList<ActiveInfo> activeDistribution = new ArrayList<>();
      final ArrayList<GrowthInfo> growthDistribution = new ArrayList<>();
      for (int j = 0; j < current.municipalityDistribution().size(); j++) {
        final HospitalisationInfo currentVirusInfo = current.municipalityDistribution().get(j);
        final HospitalisationInfo previousVirusInfo = previousDistribution.get(currentVirusInfo.code());

        final EvictingQueue<Integer> evictingQueue;
        if (!activeDistributionTracker.containsKey(currentVirusInfo.code())) {
          evictingQueue = EvictingQueue.create(CUMULATIVE_EVICTION_DAYS);
          activeDistributionTracker.put(currentVirusInfo.code(), evictingQueue);
        } else {
          evictingQueue = activeDistributionTracker.get(currentVirusInfo.code());
        }

        final ActiveInfo.Builder activeBldr = ActiveInfo.builder()
            .code(currentVirusInfo.code());
        final GrowthInfo.Builder growthBldr = GrowthInfo.builder()
            .code(currentVirusInfo.code());

        int amount;
        if (previousVirusInfo == null || previousVirusInfo.amount() == 0) {
          amount = currentVirusInfo.amount();
          growthBldr.factor(null);
        } else {
          amount = currentVirusInfo.amount() - previousVirusInfo.amount();
          growthBldr.factor((double) currentVirusInfo.amount() / previousVirusInfo.amount());
        }
        setActiveAmount += amount;

        growthBldr.amount(amount);
        evictingQueue.add(amount);

        final int activeSum = evictingQueue.stream().mapToInt(v -> v).sum();
        activeBldr.amount(activeSum);

        activeDistribution.add(activeBldr.build());
        growthDistribution.add(growthBldr.build());
      }
      totalActiveTracker.add(setActiveAmount);
      target.growth(calculateGrowth(
          current.municipalityDistribution().stream()
              .mapToInt(v -> v.amount()).sum(),
          previousDistribution.values().stream()
              .mapToInt(v -> v.amount()).sum()));
      target.active(totalActiveTracker.stream().mapToInt(v -> v).sum());

      target.municipalActiveDistribution(activeDistribution);
      target.municipalGrowthDistribution(growthDistribution);
      previousDistribution = current.municipalityDistribution().stream()
          .collect(Collectors.toMap(v -> v.code(), v -> v));
    }

    return lst.stream()
        .map(v -> v.build())
        .collect(Collectors.toList());
  }

  private List<IndexedCasesDistributionSet> provisionCases(final List<CasesDistributionSet> data) {
    final List<IndexedCasesDistributionSet.Builder> lst = new ArrayList<>(data.size());
    data.forEach(item -> {
      lst.add(IndexedCasesDistributionSet.builder()
          .date(item.date())
          .total(item.total())
          .source(item.source())
          .municipalityDistribution(item.municipalityDistribution())
          .provinceDistribution(item.provinceDistribution()));
    });

    // Horrible
    final java.util.Map<String, EvictingQueue<Integer>> activeDistributionTracker = new HashMap<>();
    java.util.Map<String, VirusInfo> previousDistribution = new HashMap<>();
    final EvictingQueue<Integer> totalActiveTracker = EvictingQueue.create(CUMULATIVE_EVICTION_DAYS);
    for (int i = 0; i < data.size(); i++) {
      final CasesDistributionSet current = data.get(i);

      final IndexedCasesDistributionSet.Builder target = lst.get(i);

      int setActiveAmount = 0;
      final ArrayList<ActiveInfo> activeDistribution = new ArrayList<>();
      final ArrayList<GrowthInfo> growthDistribution = new ArrayList<>();
      for (int j = 0; j < current.municipalityDistribution().size(); j++) {
        final VirusInfo currentVirusInfo = current.municipalityDistribution().get(j);
        final VirusInfo previousVirusInfo = previousDistribution.get(currentVirusInfo.code());

        final EvictingQueue<Integer> evictingQueue;
        if (!activeDistributionTracker.containsKey(currentVirusInfo.code())) {
          evictingQueue = EvictingQueue.create(CUMULATIVE_EVICTION_DAYS);
          activeDistributionTracker.put(currentVirusInfo.code(), evictingQueue);
        } else {
          evictingQueue = activeDistributionTracker.get(currentVirusInfo.code());
        }

        final ActiveInfo.Builder activeBldr = ActiveInfo.builder()
            .code(currentVirusInfo.code());
        final GrowthInfo.Builder growthBldr = GrowthInfo.builder()
            .code(currentVirusInfo.code());

        int amount;

        if (previousVirusInfo == null || previousVirusInfo.amount() == 0) {
          amount = 0;
          growthBldr.factor(null);
        } else {
          amount = currentVirusInfo.amount() - previousVirusInfo.amount();
          growthBldr.factor((double) currentVirusInfo.amount() / previousVirusInfo.amount());
        }
        setActiveAmount += amount;

        growthBldr.amount(amount);
        evictingQueue.add(amount);

        final int activeSum = evictingQueue.stream().mapToInt(v -> v).sum();
        activeBldr.amount(activeSum);

        activeDistribution.add(activeBldr.build());
        growthDistribution.add(growthBldr.build());
      }

      totalActiveTracker.add(setActiveAmount);
      target.growth(calculateGrowth(
          current.municipalityDistribution().stream()
              .mapToInt(v -> v.amount()).sum(),
          previousDistribution.values().stream()
              .mapToInt(v -> v.amount()).sum()));
      target.active(totalActiveTracker.stream().mapToInt(v -> v).sum());

      target.municipalActiveDistribution(activeDistribution);
      target.municipalGrowthDistribution(growthDistribution);
      previousDistribution = current.municipalityDistribution().stream()
          .collect(Collectors.toMap(v -> v.code(), v -> v));
    }

    return lst.stream()
        .map(v -> v.build())
        .skip(14)
        .collect(Collectors.toList());
  }

  @EventHandler
  public void onVirusDataTypeChangedCommand(final VirusDataTypeChangedCommand e) {
    setDataType(e.getValue());
  }

  @EventHandler
  public void onActiveHospitalisationDistributionSetChangeCommand(final ActiveHospitalisationDistributionSetChangeCommand c) {
    municipalityActiveHospitalisationLayer.displayAreaHighlight(c.getValue().municipalActiveDistribution().stream()
        .collect(Collectors.toMap(v -> v.code(), v -> v)));
  }

  @EventHandler
  public void onHospitalisationDistributionSetChangeCommand(final HospitalisationDistributionSetChangeCommand c) {
    municipalityHospitalisationLayer.displayAreaHighlight(c.getValue().municipalityDistribution().stream()
        .collect(Collectors.toMap(v -> v.code(), v -> v)));
  }

  @EventHandler
  public void onFatalityDistributionSetChangeCommand(final FatalityDistributionSetChangeCommand c) {
    municipalityFatalitiesLayer.displayAreaHighlight(c.getValue().municipalityDistribution().stream()
        .collect(Collectors.toMap(v -> v.code(), v -> v)));
  }

  @EventHandler
  public void onActiveCasesDistributionSetChangeCommand(final ActiveCasesDistributionSetChangeCommand c) {
    municipalityActiveCasesLayer.displayAreaHighlight(c.getValue().municipalActiveDistribution().stream()
        .collect(Collectors.toMap(v -> v.code(), v -> v)));
  }

  @EventHandler
  public void onVirusDistributionSetChangeCommand(final CasesDistributionSetChangeCommand c) {
    municipalityCasesLayer.displayAreaHighlight(c.getValue().municipalityDistribution().stream()
        .collect(Collectors.toMap(v -> v.code(), v -> v)));
  }

  @Override
  public void notifyMap(final Map mapHandle, final MapEventBus mapEventBus) {
    this.map = mapHandle.getMap();
    this.mapEventBus = mapEventBus;
    final Projection projection = Projection.get(epsg.getEpsgCode());

    if (municipalityCasesLayer == null) {
      municipalityCasesLayer = new MunicipalityCasesLayer(map, projection, eventBus);
      municipalityCasesLayer.setSelectedConsumer(area -> {
        eventBus.fireEvent(new MunicipalitySelectedEvent(area));
      });
      municipalityCasesLayer.setHoverConsumer(area -> {
        eventBus.fireEvent(new MunicipalityHoverEvent(area));
      });
    }

    if (municipalityActiveCasesLayer == null) {
      municipalityActiveCasesLayer = new MunicipalityActiveCasesLayer(map, projection, eventBus);
      municipalityActiveCasesLayer.setSelectedConsumer(area -> {
        eventBus.fireEvent(new MunicipalitySelectedEvent(area));
      });
      municipalityActiveCasesLayer.setHoverConsumer(area -> {
        eventBus.fireEvent(new MunicipalityHoverEvent(area));
      });
    }

    if (municipalityHospitalisationLayer == null) {
      municipalityHospitalisationLayer = new MunicipalityHospitalisationLayer(map, projection, eventBus);
      municipalityHospitalisationLayer.setSelectedConsumer(area -> {
        eventBus.fireEvent(new MunicipalitySelectedEvent(area));
      });
      municipalityHospitalisationLayer.setHoverConsumer(area -> {
        eventBus.fireEvent(new MunicipalityHoverEvent(area));
      });
    }

    if (municipalityActiveHospitalisationLayer == null) {
      municipalityActiveHospitalisationLayer = new MunicipalityActiveHospitalisationLayer(map, projection, eventBus);
      municipalityActiveHospitalisationLayer.setSelectedConsumer(area -> {
        eventBus.fireEvent(new MunicipalitySelectedEvent(area));
      });
      municipalityActiveHospitalisationLayer.setHoverConsumer(area -> {
        eventBus.fireEvent(new MunicipalityHoverEvent(area));
      });
    }

    if (municipalityFatalitiesLayer == null) {
      municipalityFatalitiesLayer = new MunicipalityFatalitiesLayer(map, projection, eventBus);
      municipalityFatalitiesLayer.setSelectedConsumer(area -> {
        eventBus.fireEvent(new MunicipalitySelectedEvent(area));
      });
      municipalityFatalitiesLayer.setHoverConsumer(area -> {
        eventBus.fireEvent(new MunicipalityHoverEvent(area));
      });
    }

    final IsLayer<Layer> luchtfotoLabels = prepareLabels(projection, epsg);
    mapEventBus.fireEvent(new LayerAddedCommand(luchtfotoLabels));

    service.getViralDistributionLimited(AppAsyncCallback.create(
        dist -> eventBus.fireEvent(new VirusDistributionChangedEvent(dist)),
        e -> eventBus.fireEvent(new VirusDistributionLoadError(e.getMessage()))));
  }

  @EventHandler
  public void loadFullDataSet(final LoadFullDataSetCommand c) {
    service.getViralDistribution(dist -> {
      eventBus.fireEvent(new VirusDistributionChangedEvent(dist));
    });
  }
}
