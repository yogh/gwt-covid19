/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.yogh.gwt.wui.covid.ui;

import com.google.inject.Inject;

import io.yogh.gwt.wui.dev.GWTProd;
import io.yogh.gwt.wui.place.Place;

import nl.yogh.gwt.wui.covid.place.CovidPlaces.GraphPlace;
import nl.yogh.gwt.wui.covid.place.CovidPlaces.InfoPlace;
import nl.yogh.gwt.wui.covid.place.CovidPlaces.MapPlace;

public class MainActivityManager extends AbstractSubActivityManager<MainView, MainSubPresenter> {
  private final MainActivityFactory activityFactory;

  @Inject
  public MainActivityManager(final MainActivityFactory activityFactory) {
    this.activityFactory = activityFactory;
  }

  @Override
  public MainSubPresenter getActivity(final Place place, final MainView view) {
    if (place instanceof MapPlace) {
      return activityFactory.createMapPresenter(view, (MapPlace) place);
    } else if (place instanceof GraphPlace) {
      return activityFactory.createGraphPresenter(view, (GraphPlace) place);
    } else if (place instanceof InfoPlace) {
      return activityFactory.createInfoPresenter(view, (InfoPlace) place);
    } else {
      GWTProd.warn("AirQualityActivityManager", "Could not create sub-activity inside AirQualityActivity: no activity for " + place);
//      return activityFactory.createEmptyActivity(view);
      return null;
    }
  }

  @Override
  protected Place getRedirect(final Place place) {
    final Place redirect = null;
    return redirect;
  }
}
