package nl.yogh.gwt.wui.covid.ui.graph;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;

import nl.yogh.gwt.wui.covid.ui.MainView;

public class GraphActivity implements GraphPresenter {
  @Inject
  public GraphActivity(@Assisted final MainView view) {
    view.setMain(GraphViewFactory.get());
  }

  @Override
  public void setView(final GraphView view) {}
}
