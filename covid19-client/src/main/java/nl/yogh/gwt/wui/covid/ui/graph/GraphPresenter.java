package nl.yogh.gwt.wui.covid.ui.graph;

import nl.yogh.gwt.wui.covid.ui.MainSubPresenter;

public interface GraphPresenter extends MainSubPresenter {
  void setView(GraphView view);
}
