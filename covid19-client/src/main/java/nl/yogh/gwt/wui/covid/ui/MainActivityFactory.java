package nl.yogh.gwt.wui.covid.ui;

import nl.yogh.gwt.wui.covid.place.CovidPlaces.GraphPlace;
import nl.yogh.gwt.wui.covid.place.CovidPlaces.InfoPlace;
import nl.yogh.gwt.wui.covid.place.CovidPlaces.MapPlace;
import nl.yogh.gwt.wui.covid.ui.graph.GraphActivity;
import nl.yogh.gwt.wui.covid.ui.info.InfoActivity;
import nl.yogh.gwt.wui.covid.ui.map.MapActivity;

public interface MainActivityFactory {
  MapActivity createMapPresenter(MainView view, MapPlace place);

  GraphActivity createGraphPresenter(MainView view, GraphPlace place);

  InfoActivity createInfoPresenter(MainView view, InfoPlace place);
}
