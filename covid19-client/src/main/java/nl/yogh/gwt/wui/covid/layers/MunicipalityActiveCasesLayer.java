package nl.yogh.gwt.wui.covid.layers;

import com.google.web.bindery.event.shared.EventBus;

import ol.OLFactory;
import ol.proj.Projection;
import ol.style.Style;
import ol.style.StyleOptions;

import nl.yogh.covid.domain.ActiveInfo;
import nl.yogh.covid.domain.MunicipalityInfo;

public class MunicipalityActiveCasesLayer extends MunicipalityLayer<ActiveInfo> {
  private static final int RELATIVE_LOG_MULTIPLIER = 25;
  private static final double PER_MULTIPLIER = 1.4;
  private static final double LOG_CASES_PER_START = 255D / PER_MULTIPLIER;

  public MunicipalityActiveCasesLayer(final ol.Map map, final Projection projection, final EventBus eventBus) {
    super(map, projection, eventBus);
  }

  @Override
  protected Style createStyle(final MunicipalityInfo municipality, final ActiveInfo virusInfo) {
    final StyleOptions styleOptions = new StyleOptions();
    styleOptions.setStroke(OLFactory.createStroke(OLFactory.createColor(214, 51, 39, 0.1), 2));

    if (virusInfo != null) {
      final double cases = virusInfo.amount();
      final double casesPerThousand = cases / municipality.population() * (100 * 1000D);

      final int r = casesPerThousand == 0 ? 0
          : casesPerThousand > LOG_CASES_PER_START ? 255 - logScale(casesPerThousand - LOG_CASES_PER_START, RELATIVE_LOG_MULTIPLIER) : 255;
      final int g = (int) Math.max(0, 255 - casesPerThousand * PER_MULTIPLIER);
      final int b = 0;

      final double a = virusInfo.amount() == 0 ? 0.1 : 0.9;
      styleOptions.setFill(OLFactory.createFill(OLFactory.createColor(r, g, b, a)));
    }

    return new Style(styleOptions);
  }

  private int logScale(final double casesPerThousand, final int multiplier) {
    final double logg = Math.log(casesPerThousand) * multiplier;

    return (int) logg;
  }
}
