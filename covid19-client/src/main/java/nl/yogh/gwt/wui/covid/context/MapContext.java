package nl.yogh.gwt.wui.covid.context;

import com.google.inject.ImplementedBy;
import com.google.web.bindery.event.shared.HandlerRegistration;

import io.yogh.gwt.geo.domain.IsMapCohort;
import io.yogh.gwt.geo.wui.Map;

@ImplementedBy(MapContextImpl.class)
public interface MapContext {
  default HandlerRegistration onMap(final IsMapCohort cohort) {
    return registerPrimaryMapCohort(cohort);
  }

  HandlerRegistration registerPrimaryMapCohort(IsMapCohort cohort);

  void claimMapPrimacy(Map map);

  Map getActiveMap();
}
