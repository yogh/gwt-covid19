package nl.yogh.gwt.wui.component;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.google.gwt.safehtml.shared.SimpleHtmlSanitizer;

import io.yogh.gwt.vue.component.BasicVueComponent;

import jsinterop.annotations.JsMethod;

import nl.yogh.gwt.wui.i18n.ApplicationMessages;
import nl.yogh.gwt.wui.i18n.M;
import nl.yogh.gwt.wui.resources.ApplicationColors;
import nl.yogh.gwt.wui.resources.ImageResources;
import nl.yogh.gwt.wui.resources.R;

@Component(hasTemplate = false)
public abstract class CovidVueComponent extends BasicVueComponent implements IsVueComponent {
  @Data public ApplicationMessages i18n = M.messages();

  @Data public ImageResources img = R.images();

  @Data public ApplicationColors colors = R.colors();
  
  @JsMethod
  public String toLocaleString(final String num) {
    return toLocaleString(Double.parseDouble(num));
  }

  @JsMethod
  public String toFixed(final String num, final int length) {
    return toFixed(num, length);
  }
  
  public static native String toLocaleString(double value) /*-{
    return value.toLocaleString();
  }-*/;
  
  public static native String toFixed(double value, int length) /*-{
    return value.toFixed(length); 
  }-*/;

  @JsMethod
  public String sanitize(final String html) {
    return SimpleHtmlSanitizer.sanitizeHtml(html).asString();
  }
}
