package nl.yogh.gwt.wui.covid.event;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

import nl.yogh.covid.domain.ProvinceInfo;

public class ProvinceSelectedEvent extends SimpleGenericEvent<ProvinceInfo> {
  public ProvinceSelectedEvent(final ProvinceInfo code) {
    super(code);
  }
}
