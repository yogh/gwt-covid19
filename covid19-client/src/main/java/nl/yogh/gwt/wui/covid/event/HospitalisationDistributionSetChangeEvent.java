package nl.yogh.gwt.wui.covid.event;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

import nl.yogh.covid.domain.IndexedHospitalisationDistributionSet;

public class HospitalisationDistributionSetChangeEvent extends SimpleGenericEvent<IndexedHospitalisationDistributionSet> {
  public HospitalisationDistributionSetChangeEvent(final IndexedHospitalisationDistributionSet value) {
    super(value);
  }
}
