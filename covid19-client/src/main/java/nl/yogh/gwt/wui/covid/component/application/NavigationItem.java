package nl.yogh.gwt.wui.covid.component.application;

import static com.axellience.vuegwt.core.client.tools.JsUtils.e;
import static com.axellience.vuegwt.core.client.tools.JsUtils.map;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;

import io.yogh.gwt.wui.place.ApplicationPlace;
import io.yogh.gwt.wui.place.Place;

import jsinterop.annotations.JsMethod;
import jsinterop.base.JsPropertyMap;

import nl.yogh.gwt.wui.component.CovidVueComponent;

@Component
public class NavigationItem extends CovidVueComponent implements IsVueComponent {
  @Prop ApplicationPlace place;
  @Prop Place activePlace;

  @Data JsPropertyMap<String> activeStyle;

  @JsMethod
  public void select() {
    vue().$emit("select", place);
  }

  @Watch(value = "activePlace", isImmediate = true)
  public void onActivePlaceChange() {
    if (isActive()) {
      activeStyle = map(
          e("color", colors.placeFontColor(activePlace.getClass().getSimpleName())),
          e("background", colors.placeBackgroundColor(activePlace.getClass().getSimpleName())));
    } else {
      activeStyle = map();
    }
  }

  @Computed("active")
  public boolean isActive() {
    return activePlace != null && activePlace.getClass().equals(place.getClass());
  }
}
