package nl.yogh.gwt.wui.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.DataResource.MimeType;

/**
 * Image Resource interface to all images.
 */
public interface ImageResources extends ClientBundle {
  @Source("images/icons/baseline-menu-24px.svg")
  @MimeType("image/svg+xml")
  DataResource baselineMenu();

  @Source("images/icons/baseline-library_books-24px.svg")
  @MimeType("image/svg+xml")
  DataResource baselineLibrary();

  @Source("images/icons/baseline-next_week-24px.svg")
  @MimeType("image/svg+xml")
  DataResource baselineNextWeek();

  @Source("images/icons/baseline-settings-20px.svg")
  @MimeType("image/svg+xml")
  DataResource baselineSettings();
}
