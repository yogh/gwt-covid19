package nl.yogh.gwt.wui.covid.command;

import io.yogh.gwt.wui.command.SimpleGenericCommand;

import nl.yogh.covid.domain.IndexedHospitalisationDistributionSet;
import nl.yogh.gwt.wui.covid.event.ActiveHospitalisationDistributionSetChangeEvent;

public class ActiveHospitalisationDistributionSetChangeCommand
    extends SimpleGenericCommand<IndexedHospitalisationDistributionSet, ActiveHospitalisationDistributionSetChangeEvent> {
  public ActiveHospitalisationDistributionSetChangeCommand(final IndexedHospitalisationDistributionSet value) {
    super(value);
  }

  @Override
  protected ActiveHospitalisationDistributionSetChangeEvent createEvent(final IndexedHospitalisationDistributionSet value) {
    return new ActiveHospitalisationDistributionSetChangeEvent(value);
  }
}
