package nl.yogh.gwt.wui.covid.daemon;

import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.wui.daemon.DaemonBootstrapper;
import io.yogh.gwt.wui.dev.DevelopmentObserver;
import io.yogh.gwt.wui.event.BasicEventComponent;

public class CovidDaemonBootstrapper extends BasicEventComponent implements DaemonBootstrapper {
  @Inject DevelopmentObserver developmentObserver;
  @Inject ExceptionDaemon exceptionDaemon;
  @Inject VirusDistributionDaemon virusDistributionDaemon;
  @Inject MobileDaemon mobileDaemon;

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, developmentObserver, exceptionDaemon, virusDistributionDaemon, mobileDaemon);
  }
}
