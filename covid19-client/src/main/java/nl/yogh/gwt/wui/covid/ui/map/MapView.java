package nl.yogh.gwt.wui.covid.ui.map;

import java.util.Date;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Computed;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.annotations.component.Ref;
import com.axellience.vuegwt.core.annotations.component.Watch;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.axellience.vuegwt.core.client.component.hooks.HasDestroyed;
import com.axellience.vuegwt.core.client.component.hooks.HasMounted;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Document;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import io.yogh.gwt.geo.command.MapResizeCommand;
import io.yogh.gwt.geo.wui.OL3MapComponent;
import io.yogh.gwt.wui.util.SchedulerUtil;

import jsinterop.annotations.JsMethod;

import nl.yogh.covid.domain.IndexedCasesDistributionSet;
import nl.yogh.covid.domain.IndexedDistributionSet;
import nl.yogh.covid.domain.IndexedFatalityDistributionSet;
import nl.yogh.covid.domain.IndexedHospitalisationDistributionSet;
import nl.yogh.covid.domain.MunicipalityInfo;
import nl.yogh.gwt.wui.component.CovidVueComponent;
import nl.yogh.gwt.wui.covid.command.ActiveCasesDistributionSetChangeCommand;
import nl.yogh.gwt.wui.covid.command.ActiveHospitalisationDistributionSetChangeCommand;
import nl.yogh.gwt.wui.covid.command.CasesDistributionSetChangeCommand;
import nl.yogh.gwt.wui.covid.command.FatalityDistributionSetChangeCommand;
import nl.yogh.gwt.wui.covid.command.HospitalisationDistributionSetChangeCommand;
import nl.yogh.gwt.wui.covid.command.ToggleMenuEvent;
import nl.yogh.gwt.wui.covid.command.ToggleSettingsCommand;
import nl.yogh.gwt.wui.covid.context.MapContext;
import nl.yogh.gwt.wui.covid.context.VirusContext;
import nl.yogh.gwt.wui.covid.context.VirusContext.DataType;
import nl.yogh.gwt.wui.covid.daemon.LoadFullDataSetCommand;
import nl.yogh.gwt.wui.covid.daemon.VirusDistributionLoadError;
import nl.yogh.gwt.wui.covid.event.ActiveCasesDistributionSetChangeEvent;
import nl.yogh.gwt.wui.covid.event.ActiveHospitalisationDistributionSetChangeEvent;
import nl.yogh.gwt.wui.covid.event.CasesDistributionSetChangeEvent;
import nl.yogh.gwt.wui.covid.event.FatalityDistributionSetChangeEvent;
import nl.yogh.gwt.wui.covid.event.HospitalisationDistributionSetChangeEvent;
import nl.yogh.gwt.wui.covid.event.MunicipalityHoverEvent;
import nl.yogh.gwt.wui.covid.event.MunicipalitySelectedEvent;

@Component(components = {
    OL3MapComponent.class
})
public class MapView extends CovidVueComponent implements IsVueComponent, HasMounted, HasCreated, HasDestroyed {
  private static final int INITIAL_PLAYBACK_DELAY = 3000;
  private static final int SMALL_INCREMENT = 100;
  private static final int LARGE_INCREMENT = 500;

  private static final LandingViewEventBinder EVENT_BINDER = GWT.create(LandingViewEventBinder.class);

  interface LandingViewEventBinder extends EventBinder<MapView> {}

  private int playbackDelay;
  @Data int timeInterval = 600;

  private HandlerRegistration resizeHandler;

  @Ref OL3MapComponent map;

  @Inject MapContext mapContext;

  @Prop EventBus eventBus;

  @Data @Inject VirusContext virusContext;

  @Data IndexedDistributionSet actualData;
  @Data IndexedCasesDistributionSet actualCasesData;
  @Data IndexedHospitalisationDistributionSet actualHospitalisationData;
  @Data IndexedFatalityDistributionSet actualFatalityData;
  @Data String actualDate;
  @Data String actualCases;
  @Data String actualGrowth;
  @Data String actualAbsoluteGrowth;

  @Data MunicipalityInfo hoveredMunicipality;
  @Data MunicipalityInfo selectedMunicipality;

  @Data boolean settingsVisible = true;
  @Data boolean infoVisible = false;

  @Data boolean playDelayActive = true;
  @Data boolean play = false;

  @Data String error;

  @Data long playDelayStart;
  @Data long playDelayCurrent;

  private int playCounter;
  
  @JsMethod
  public void closeInfo() {
    infoVisible = false;
  }

  @Watch("play")
  public void onPlayChange(final boolean neww, final boolean old) {
    if (neww) {
      playDelayActive = false;
      schedulePlay();
    }
  }

  private void schedulePlay() {
    playCounter++;
    final int localPlayCounter = playCounter;
    Scheduler.get().scheduleFixedPeriod(() -> {
      if (localPlayCounter != playCounter) {
        return false;
      }

      selectNext();
      return play;
    }, timeInterval);
  }

  @Watch("timeInterval")
  public void onIntervalChange(final int neww, final int old) {
    schedulePlay();
  }
  
  @EventHandler
  public void onVirusDistributionLoadError(final VirusDistributionLoadError e) {
    this.error = e.getValue();
  }

  @Computed
  public String getPlayDelayRemaining() {
    return "" + (int) Math.ceil((playDelayStart + playbackDelay - playDelayCurrent) / 1000D) + "s";
  }

  @JsMethod
  public void decreaseInterval() {
    setTimeInterval(timeInterval <= 2000 ? timeInterval - SMALL_INCREMENT : timeInterval - LARGE_INCREMENT);
  }

  @JsMethod
  public void increaseInterval() {
    setTimeInterval(timeInterval < 2000 ? timeInterval + SMALL_INCREMENT : timeInterval + 500);
  }

  @JsMethod
  public void setTimeInterval(final int timeInterval) {
    final int cor = timeInterval / SMALL_INCREMENT * SMALL_INCREMENT;

    this.timeInterval = Math.min(Math.max(10, cor), 50000);
  }

  @Computed
  public MunicipalityInfo getMunicipalityInfo() {
    return selectedMunicipality == null ? hoveredMunicipality : selectedMunicipality;
  }

  @JsMethod
  public void selectNext() {
    if (virusContext == null) {
      return;
    }

    if (!play) {
      return;
    }

    if (!virusContext.getActiveDistributionData().isEmpty()) {
      final int currentIndex = findCurrentIndex();
      final IndexedDistributionSet virusDistributionSet = virusContext.getActiveDistributionData()
          .get(currentIndex + 1 >= virusContext.getActiveDistributionData().size() ? 0 : currentIndex + 1);
      internalSelectDataset(virusContext.getActive(), virusDistributionSet);
    }
  }
  
  @JsMethod
  public void loadAll() {
    eventBus.fireEvent(new LoadFullDataSetCommand());
  }

  private int findCurrentIndex() {
    if (virusContext.getActiveDistributionData() == null) {
      return 0;
    }

    for (int i = 0; i < virusContext.getActiveDistributionData().size(); i++) {
      final IndexedDistributionSet activeSet = virusContext.getActiveDistributionData().get(i);
      if (activeSet == actualCasesData
          || activeSet == actualHospitalisationData
          || activeSet == actualFatalityData) {
        return i;
      }
    }

    return 0;
  }

  @Computed
  public String getSelectedAbsoluteGrowth() {
    if (getMunicipalityInfo() == null) {
      return formatGrowth(0D);
    }

    final DataType data = virusContext.getActive();
    if (data == DataType.ACTIVE_CASES || data == DataType.ACTIVE_HOSPITALISATIONS) {
      return "";
    } else {
      return formatAbsoluteGrowth(actualData.municipalGrowthDistribution().stream()
          .filter(v -> v.code().equals(getMunicipalityInfo().code()))
          .findFirst()
          .map(v -> v.amount())
          .orElse(0));
    }
  }

  @Computed
  public String getSelectedGrowth() {
    if (getMunicipalityInfo() == null) {
      return formatGrowth(0D);
    }

    final DataType data = virusContext.getActive();
    if (data == DataType.ACTIVE_CASES || data == DataType.ACTIVE_HOSPITALISATIONS) {
      return "";
    } else {
      return formatGrowth(actualData.municipalGrowthDistribution().stream()
          .filter(v -> v.code().equals(getMunicipalityInfo().code()))
          .findFirst()
          .map(v -> v.factor())
          .orElse(0D));
    }
  }

  @Computed
  public String getSelectedCases() {
    if (getMunicipalityInfo() == null) {
      return "";
    }

    final DataType data = virusContext.getActive();
    if (data == DataType.ACTIVE_CASES || data == DataType.ACTIVE_HOSPITALISATIONS) {
      return String.valueOf(actualData.municipalActiveDistribution().stream()
          .filter(v -> v.code().equals(getMunicipalityInfo().code()))
          .findFirst().get().amount());
    } else {
      return String.valueOf(actualData.municipalityDistribution().stream()
          .filter(v -> v.code().equals(getMunicipalityInfo().code()))
          .findFirst().get().amount());
    }
  }

  @Computed("hasData")
  public boolean hasData() {
    return !virusContext.getCasesDistributionData().isEmpty();
  }

  @EventHandler
  public void onMunicipalitySelectedEvent(final MunicipalitySelectedEvent e) {
    if (e.getValue() == null) {
      selectedMunicipality = null;
      return;
    }

    if (selectedMunicipality != null && selectedMunicipality.code().equals(e.getValue().code())) {
      selectedMunicipality = null;
    } else {
      selectedMunicipality = e.getValue();
    }
  }

  @EventHandler
  public void onToggleMenuEvent(final ToggleMenuEvent e) {
    if (e.getValue()) {
      settingsVisible = false;
    }
  }

  @EventHandler
  public void onMunicipalitySelectedEvent(final MunicipalityHoverEvent e) {
    hoveredMunicipality = e.getValue();
  }

  @EventHandler
  public void onToggleSettingsCommand(final ToggleSettingsCommand c) {
    settingsVisible = !settingsVisible;
    c.settle(settingsVisible);
  }

  private String formatGrowth(final Double growth) {
    final double percentage = (growth - 1) * 100;
    return (percentage >= 0 ? "+" : "-") + toFixed(percentage, 1) + "%";
  }

  private String formatAbsoluteGrowth(final int growth) {
    return (growth >= 0 ? "+" : "-") + toFixed(growth, 0);
  }

  @EventHandler
  public void onActiveHospitalisationDistributionSetChangeEvent(final ActiveHospitalisationDistributionSetChangeEvent e) {
    actualData = e.getValue();
    actualHospitalisationData = e.getValue();
    actualDate = formatDate(actualHospitalisationData.date());
    actualCases = "" + actualHospitalisationData.active();
    actualGrowth = "";
    actualAbsoluteGrowth = "";

    if (!play) {
      return;
    }

    // If the selected dataset is the last, stop playback and delay
    if (e.getValue().equals(virusContext.getHospitalisationDistributionData().get(virusContext.getHospitalisationDistributionData().size() - 1))) {
      schedulePlaybackDelay((int) (Math.ceil(timeInterval * 3D / 1000D) * 1000));
    }
  }

  @EventHandler
  public void onHospitalisationDistributionSetChangeEvent(final HospitalisationDistributionSetChangeEvent e) {
    actualData = e.getValue();
    actualHospitalisationData = e.getValue();
    actualDate = formatDate(actualHospitalisationData.date());
    actualCases = "" + actualHospitalisationData.total();
    actualGrowth = formatGrowth(actualHospitalisationData.growth());
    actualAbsoluteGrowth = formatAbsoluteGrowth(actualHospitalisationData.total());

    if (!play) {
      return;
    }

    // If the selected dataset is the last, stop playback and delay
    if (e.getValue().equals(virusContext.getHospitalisationDistributionData().get(virusContext.getHospitalisationDistributionData().size() - 1))) {
      schedulePlaybackDelay((int) (Math.ceil(timeInterval * 3D / 1000D) * 1000));
    }
  }

  @EventHandler
  public void onActiveVirusDistributionSetChangeEvent(final ActiveCasesDistributionSetChangeEvent e) {
    actualData = e.getValue();
    actualCasesData = e.getValue();
    actualDate = formatDate(actualCasesData.date());
    actualCases = "" + actualCasesData.active();
    actualGrowth = "";

    if (!play) {
      return;
    }

    // If the selected dataset is the last, stop playback and delay
    if (e.getValue().equals(virusContext.getCasesDistributionData().get(virusContext.getCasesDistributionData().size() - 1))) {
      schedulePlaybackDelay((int) (Math.ceil(timeInterval * 3D / 1000D) * 1000));
    }
  }

  @EventHandler
  public void onVirusDistributionSetChangeEvent(final CasesDistributionSetChangeEvent e) {
    actualData = e.getValue();
    actualCasesData = e.getValue();
    actualDate = formatDate(actualCasesData.date());
    actualCases = "" + actualCasesData.total();
    actualGrowth = formatGrowth(actualCasesData.growth());

    if (!play) {
      return;
    }

    // If the selected dataset is the last, stop playback and delay
    if (e.getValue().equals(virusContext.getCasesDistributionData().get(virusContext.getCasesDistributionData().size() - 1))) {
      schedulePlaybackDelay((int) (Math.ceil(timeInterval * 3D / 1000D) * 1000));
    }
  }

  @EventHandler
  public void onVirusFatalitySetChangeEvent(final FatalityDistributionSetChangeEvent e) {
    actualData = e.getValue();
    actualFatalityData = e.getValue();
    actualDate = formatDate(actualFatalityData.date());
    actualCases = "" + actualFatalityData.total();
    actualGrowth = formatGrowth(actualFatalityData.growth());

    if (!play) {
      return;
    }

    // If the selected dataset is the last, stop playback and delay
    if (e.getValue().equals(virusContext.getFatalityDistributionData().get(virusContext.getFatalityDistributionData().size() - 1))) {
      schedulePlaybackDelay((int) (Math.ceil(timeInterval * 3D / 1000D) * 1000));
    }
  }

  @JsMethod
  public String formatDate(final long date) {
    final DateTimeFormat format = DateTimeFormat.getFormat("d MMMM y");

    return format.format(new Date(date * 1000));
  }

  @JsMethod
  public boolean isHospActive(final IndexedHospitalisationDistributionSet dataset) {
    return actualHospitalisationData != null && actualHospitalisationData.source().equals(dataset.source());
  }

  @JsMethod
  public boolean isFatlActive(final IndexedFatalityDistributionSet dataset) {
    return actualFatalityData != null && actualFatalityData.source().equals(dataset.source());
  }

  @JsMethod
  public boolean isCasesActive(final IndexedCasesDistributionSet dataset) {
    return actualCasesData != null && actualCasesData.source().equals(dataset.source());
  }

  @JsMethod
  public boolean isActive(final DataType type) {
    return virusContext.isActive(type);
  }

  @JsMethod
  public void selectHospDataset(final IndexedHospitalisationDistributionSet set) {
    play = false;
    playDelayActive = false;
    internalSelectHospDataset(set);
  }

  @JsMethod
  public void selectActiveHospDataset(final IndexedHospitalisationDistributionSet set) {
    play = false;
    playDelayActive = false;
    internalSelectActiveHospDataset(set);
  }

  @JsMethod
  public void selectFatlDataset(final IndexedFatalityDistributionSet set) {
    play = false;
    playDelayActive = false;
    internalSelectFatlDataset(set);
  }

  @JsMethod
  public void selectActiveCasesDataset(final IndexedCasesDistributionSet set) {
    play = false;
    playDelayActive = false;
    internalSelectDataset(virusContext.getActive(), set);
  }

  @JsMethod
  public void selectCasesDataset(final IndexedCasesDistributionSet set) {
    play = false;
    playDelayActive = false;
    internalSelectDataset(virusContext.getActive(), set);
  }

  @JsMethod
  private void internalSelectDataset(final DataType dataType, final IndexedDistributionSet set) {
    if (set instanceof IndexedHospitalisationDistributionSet) {
      if (dataType == DataType.ACTIVE_HOSPITALISATIONS) {
        internalSelectActiveHospDataset((IndexedHospitalisationDistributionSet) set);
      } else {
        internalSelectHospDataset((IndexedHospitalisationDistributionSet) set);
      }
    } else if (set instanceof IndexedCasesDistributionSet) {
      if (dataType == DataType.ACTIVE_CASES) {
        internalSelectActiveCasesDataset((IndexedCasesDistributionSet) set);
      } else {
        internalSelectCasesDataset((IndexedCasesDistributionSet) set);
      }
    } else if (set instanceof IndexedFatalityDistributionSet) {
      internalSelectFatlDataset((IndexedFatalityDistributionSet) set);
    }
  }

  @JsMethod
  private void internalSelectHospDataset(final IndexedHospitalisationDistributionSet set) {
    eventBus.fireEvent(new HospitalisationDistributionSetChangeCommand(set));
  }

  @JsMethod
  private void internalSelectActiveHospDataset(final IndexedHospitalisationDistributionSet set) {
    eventBus.fireEvent(new ActiveHospitalisationDistributionSetChangeCommand(set));
  }

  @JsMethod
  private void internalSelectCasesDataset(final IndexedCasesDistributionSet set) {
    eventBus.fireEvent(new CasesDistributionSetChangeCommand(set));
  }

  @JsMethod
  private void internalSelectActiveCasesDataset(final IndexedCasesDistributionSet set) {
    eventBus.fireEvent(new ActiveCasesDistributionSetChangeCommand(set));
  }

  @JsMethod
  private void internalSelectFatlDataset(final IndexedFatalityDistributionSet set) {
    eventBus.fireEvent(new FatalityDistributionSetChangeCommand(set));
  }

  @Override
  public void created() {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
    if (play) {
      selectNext();
    }

    determineWidth();
    resizeHandler = Window.addResizeHandler(e -> {
      eventBus.fireEvent(new MapResizeCommand());

      determineWidth();
    });

    schedulePlaybackDelay(INITIAL_PLAYBACK_DELAY);
  }

  private void schedulePlaybackDelay(final int delay) {
    this.playbackDelay = delay;
    play = false;
    playDelayActive = true;
    playDelayStart = System.currentTimeMillis();
    playDelayCurrent = System.currentTimeMillis();

    updateTime();

    playCounter++;
    final int localPlayCounter = playCounter;
    SchedulerUtil.delay(() -> {
      if (localPlayCounter != playCounter) {
        return;
      }

      if (!playDelayActive) {
        return;
      }

      play = true;
    }, playbackDelay);
  }

  private void updateTime() {
    playDelayCurrent = System.currentTimeMillis();

    if (!playDelayActive) {
      return;
    }

    SchedulerUtil.delay(() -> {
      updateTime();
    }, 5);
  }

  private void determineWidth() {
    if (Document.get().getClientWidth() < 800) {
      settingsVisible = false;
    }
  }

  @Override
  public void destroyed() {
    resizeHandler.removeHandler();
  }

  @Override
  public void mounted() {
    mapContext.claimMapPrimacy(map);
  }
}
