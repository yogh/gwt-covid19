package nl.yogh.gwt.wui.covid.command;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

public class ToggleMenuEvent extends SimpleGenericEvent<Boolean> {
  public ToggleMenuEvent(final boolean value) {
    super(value);
  }
}
