package nl.yogh.gwt.wui.covid.component.application;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import io.yogh.gwt.wui.event.PlaceChangeEvent;
import io.yogh.gwt.wui.place.ApplicationPlace;
import io.yogh.gwt.wui.place.Place;
import io.yogh.gwt.wui.place.PlaceController;

import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsProperty;

import nl.yogh.gwt.wui.component.CovidVueComponent;
import nl.yogh.gwt.wui.component.material.MaterialIcon;
import nl.yogh.gwt.wui.covid.command.ToggleMenuCommand;
import nl.yogh.gwt.wui.covid.command.ToggleSettingsEvent;
import nl.yogh.gwt.wui.covid.context.VirusContext.DataType;
import nl.yogh.gwt.wui.covid.event.VirusDataTypeChangedCommand;
import nl.yogh.gwt.wui.covid.place.CovidPlaces;

@Component(name = "app-navigation", components = {
    MaterialIcon.class,
    NavigationItem.class
})
public class ApplicationNavigation extends CovidVueComponent implements IsVueComponent, HasCreated {
  private static final ApplicationNavigationEventBinder EVENT_BINDER = GWT.create(ApplicationNavigationEventBinder.class);

  interface ApplicationNavigationEventBinder extends EventBinder<ApplicationNavigation> {}

  @Inject PlaceController placeController;

  @Data @JsProperty List<ApplicationPlace> items = new ArrayList<>();
  @Data @JsProperty List<ApplicationPlace> secondary = new ArrayList<>();

  @Data Place activePlace;

  @Prop EventBus eventBus;

  @Data boolean active;

  @Data DataType activeType;

  @Override
  public void created() {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
    items.add(new CovidPlaces.MapPlace());
//    items.add(new CovidPlaces.GraphPlace());
    items.add(new CovidPlaces.InfoPlace());

    setPlace(placeController.getPlace());
  }

  @JsMethod
  public void selectDataType(final DataType type) {
    eventBus.fireEvent(new VirusDataTypeChangedCommand(type));
  }

  @EventHandler
  public void onVirusDataTypeChangedCommand(final VirusDataTypeChangedCommand c) {
    activeType = c.getValue();
  }

  @EventHandler
  public void onToggleSettingsEvent(final ToggleSettingsEvent e) {
    if (e.getValue()) {
      active = false;
    }
  }

  @EventHandler
  public void onToggleMenuCommand(final ToggleMenuCommand c) {
    active = !active;
    c.settle(active);
  }

  @EventHandler
  public void onPlaceChangeEvent(final PlaceChangeEvent e) {
    setPlace(e.getValue());
  }

  private void setPlace(final Place place) {
    this.activePlace = place;
  }

  @JsMethod
  public void visitPlace(final ApplicationPlace place) {
    placeController.goTo(place);
    active = false;
  }

  @JsMethod
  public boolean isActive(final ApplicationPlace place) {
    return placeController.getPlace().getClass().equals(place.getClass());
  }
}
