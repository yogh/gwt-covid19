package nl.yogh.gwt.wui.covid.component.application;

import javax.inject.Inject;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasCreated;
import com.google.gwt.core.client.GWT;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import io.yogh.gwt.wui.dev.GWTProd;
import io.yogh.gwt.wui.event.PlaceChangeEvent;
import io.yogh.gwt.wui.place.Place;
import io.yogh.gwt.wui.place.PlaceController;

import jsinterop.annotations.JsMethod;

import nl.yogh.gwt.wui.component.CovidVueComponent;
import nl.yogh.gwt.wui.covid.command.ToggleMenuCommand;
import nl.yogh.gwt.wui.covid.command.ToggleSettingsCommand;
import nl.yogh.gwt.wui.covid.context.VirusContext.DataType;
import nl.yogh.gwt.wui.covid.daemon.VirusDistributionDaemon;
import nl.yogh.gwt.wui.covid.event.MunicipalityHoverEvent;
import nl.yogh.gwt.wui.covid.event.VirusDataTypeChangedCommand;
import nl.yogh.gwt.wui.i18n.M;
import nl.yogh.gwt.wui.resources.R;

@Component(name = "app-header")
public class ApplicationHeader extends CovidVueComponent implements IsVueComponent, HasCreated {
  private static final ApplicationHeaderEventBinder EVENT_BINDER = GWT.create(ApplicationHeaderEventBinder.class);

  interface ApplicationHeaderEventBinder extends EventBinder<ApplicationHeader> {}

  @Data String pageTitle;

  @Prop EventBus eventBus;

  @Data String backgroundColor;
  @Data String textColor;

  @Inject PlaceController placeController;

  @Inject EventBus globalEventBus;

  @Data boolean showStylingType = false;
  @Data DataType type = VirusDistributionDaemon.DEFAULT_DATATYPE;

  @Override
  public void created() {
    EVENT_BINDER.bindEventHandlers(this, eventBus);
    setPlace(placeController.getPlace());
  }

  @JsMethod
  public void clear() {
    eventBus.fireEvent(new MunicipalityHoverEvent(null));
  }

  @JsMethod
  public void popupPalette() {
    showStylingType = true;

    DataType nextType;
    switch (type) {
    case CUMULATIVE_CASES:
      nextType = DataType.ACTIVE_CASES;
      break;
    case ACTIVE_CASES:
      nextType = DataType.CUMULATIVE_HOSPITALISATIONS;
      break;
    case CUMULATIVE_HOSPITALISATIONS:
      nextType = DataType.CUMULATIVE_FATALITIES;
      break;
//    case ACTIVE_HOSPITALISATIONS:
//      nextType = DataType.CUMULATIVE_FATALITIES;
//      break;
    default:
    case CUMULATIVE_FATALITIES:
      nextType = DataType.CUMULATIVE_CASES;
      break;
    }

    type = nextType;
    globalEventBus.fireEvent(new VirusDataTypeChangedCommand(type));
  }

  @JsMethod
  public void popupMenu() {
    globalEventBus.fireEvent(new ToggleMenuCommand());
  }

  @JsMethod
  public void popupSettings() {
    globalEventBus.fireEvent(new ToggleSettingsCommand());
  }

  @JsMethod
  public void toggleMenu() {
    GWTProd.log("Toggline menu!");
  }

  @EventHandler
  public void onPlaceChange(final PlaceChangeEvent e) {
    setPlace(e.getValue());
  }

  private void setPlace(final Place value) {
    if (value == null) {
      return;
    }

    final String clazz = value.getClass().getSimpleName();

    pageTitle = M.messages().placeTitle(clazz);
    backgroundColor = R.colors().placeBackgroundColor(clazz);
    textColor = R.colors().placeFontColor(clazz);
  }
}
