package nl.yogh.gwt.wui.covid.layers;

import com.google.web.bindery.event.shared.EventBus;

import ol.Map;
import ol.proj.Projection;

import nl.yogh.covid.domain.ProvinceInfo;

public abstract class ProvinceLayer<V> extends AreaHighlightLayer<ProvinceInfo, V> {
  public ProvinceLayer(final Map map, final Projection projection, final EventBus eventBus) {
    super(map, projection, eventBus);
  }

  @Override
  protected String getGeometry(final ProvinceInfo area) {
    return area.geometry();
  }

  @Override
  protected String getKey(final ProvinceInfo area) {
    return area.code();
  }
}
