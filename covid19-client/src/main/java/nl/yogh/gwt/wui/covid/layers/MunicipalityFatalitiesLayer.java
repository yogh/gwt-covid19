package nl.yogh.gwt.wui.covid.layers;

import com.google.web.bindery.event.shared.EventBus;

import ol.OLFactory;
import ol.proj.Projection;
import ol.style.Style;
import ol.style.StyleOptions;

import nl.yogh.covid.domain.FatalityInfo;
import nl.yogh.covid.domain.MunicipalityInfo;

public class MunicipalityFatalitiesLayer extends MunicipalityLayer<FatalityInfo> {
  private static final int HALF = 127;
  private static final int RELATIVE_LOG_MULTIPLIER = 15;
  private static final double PER_MULTIPLIER = 1.2;
  private static final double LOG_CASES_PER_START = 127D / PER_MULTIPLIER;

  public MunicipalityFatalitiesLayer(final ol.Map map, final Projection projection, final EventBus eventBus) {
    super(map, projection, eventBus);
  }

  @Override
  protected Style createStyle(final MunicipalityInfo municipality, final FatalityInfo hospInfo) {
    final StyleOptions styleOptions = new StyleOptions();
    styleOptions.setStroke(OLFactory.createStroke(OLFactory.createColor(214, 51, 39, 0.1), 2));

    if (hospInfo != null) {
      final double cases = hospInfo.amount();
      final double casesPerThousand = cases / (municipality.population() / (100 * 1000D));

      final int gFirst = (int) Math.max(HALF, 255 - casesPerThousand * PER_MULTIPLIER);

      final int gSecond = casesPerThousand == 0 ? 0
          : casesPerThousand > LOG_CASES_PER_START ? logScale(casesPerThousand - LOG_CASES_PER_START, RELATIVE_LOG_MULTIPLIER) : 0;

      final int g = gFirst - gSecond;

      final double a = casesPerThousand == 0 ? 0.1 : 0.9;
      styleOptions.setFill(OLFactory.createFill(OLFactory.createColor(g, g, g, a)));
    }

    return new Style(styleOptions);
  }

  private int logScale(final double casesPerThousand, final int multiplier) {
    final double logg = Math.log(casesPerThousand) * multiplier;

    return (int) logg;
  }
}
