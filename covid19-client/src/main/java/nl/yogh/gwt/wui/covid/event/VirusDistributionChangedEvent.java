package nl.yogh.gwt.wui.covid.event;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

import nl.yogh.covid.domain.VirusDistributionInfo;

public class VirusDistributionChangedEvent extends SimpleGenericEvent<VirusDistributionInfo> {
  public VirusDistributionChangedEvent(final VirusDistributionInfo value) {
    super(value);
  }
}
