package nl.yogh.gwt.wui.covid.event;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

import nl.yogh.covid.domain.IndexedFatalityDistributionSet;

public class FatalityDistributionSetChangeEvent extends SimpleGenericEvent<IndexedFatalityDistributionSet> {
  public FatalityDistributionSetChangeEvent(final IndexedFatalityDistributionSet value) {
    super(value);
  }
}
