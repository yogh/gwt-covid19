package nl.yogh.gwt.wui;

import com.google.gwt.core.client.GWT;

public class Application {
  public static final Application A = GWT.create(Application.class);

  public void create(final Runnable finish) {
    throw new RuntimeException("No Application implementation injected.");
  }
}
