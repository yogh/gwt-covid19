package nl.yogh.gwt.wui.covid.event;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

import nl.yogh.covid.domain.MunicipalityInfo;

public class MunicipalitySelectedEvent extends SimpleGenericEvent<MunicipalityInfo> {
  public MunicipalitySelectedEvent(final MunicipalityInfo code) {
    super(code);
  }
}
