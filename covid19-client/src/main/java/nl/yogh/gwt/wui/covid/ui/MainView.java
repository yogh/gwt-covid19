package nl.yogh.gwt.wui.covid.ui;

import com.axellience.vuegwt.core.annotations.component.Component;
import com.axellience.vuegwt.core.annotations.component.Data;
import com.axellience.vuegwt.core.annotations.component.Prop;
import com.axellience.vuegwt.core.client.component.IsVueComponent;
import com.axellience.vuegwt.core.client.component.hooks.HasActivated;
import com.axellience.vuegwt.core.client.vue.VueComponentFactory;
import com.google.web.bindery.event.shared.EventBus;

import nl.yogh.gwt.wui.covid.component.application.ApplicationView;
import nl.yogh.gwt.wui.covid.ui.graph.GraphView;
import nl.yogh.gwt.wui.covid.ui.info.InfoView;
import nl.yogh.gwt.wui.covid.ui.map.MapView;

@Component(components = {
    ApplicationView.class,
    MapView.class,
    GraphView.class,
    InfoView.class,
})
public class MainView implements IsVueComponent, HasActivated {
  @Data String main;

  @Prop EventBus eventBus;

  @Prop MainPresenter presenter;

  public void setMain(final VueComponentFactory<?> fact) {
    main = fact.getComponentTagName();
  }

  @Override
  public void activated() {
    presenter.setView(this);
  }
}
