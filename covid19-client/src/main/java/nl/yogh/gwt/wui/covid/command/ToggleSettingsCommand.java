package nl.yogh.gwt.wui.covid.command;

import io.yogh.gwt.wui.command.SimpleCommand;

public class ToggleSettingsCommand extends SimpleCommand<ToggleSettingsEvent> {
  private boolean open;

  @Override
  protected ToggleSettingsEvent createEvent() {
    return new ToggleSettingsEvent(open);
  }

  public void settle(final boolean open) {
    this.open = open;
  }
}
