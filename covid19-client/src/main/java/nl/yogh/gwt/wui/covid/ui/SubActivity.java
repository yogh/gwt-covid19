package nl.yogh.gwt.wui.covid.ui;

public interface SubActivity {
  default void onStart() {
    // Default to no-op
  }
}
