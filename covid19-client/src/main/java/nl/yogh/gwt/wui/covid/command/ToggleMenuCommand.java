package nl.yogh.gwt.wui.covid.command;

import io.yogh.gwt.wui.command.SimpleCommand;

public class ToggleMenuCommand extends SimpleCommand<ToggleMenuEvent> {
  private boolean open;

  @Override
  protected ToggleMenuEvent createEvent() {
    return new ToggleMenuEvent(open);
  }

  public void settle(final boolean open) {
    this.open = open;
  }
}
