package nl.yogh.gwt.wui.covid.ui.map;

import nl.yogh.gwt.wui.covid.ui.MainSubPresenter;

public interface MapPresenter extends MainSubPresenter {
  void setView(MapView view);
}
