/*
 * Copyright the State of the Netherlands
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package nl.yogh.gwt.wui.covid.place;

import io.yogh.gwt.wui.place.ApplicationPlace;
import io.yogh.gwt.wui.place.PlaceTokenizer;

import nl.yogh.gwt.wui.covid.context.VirusContext.DataType;

public final class CovidPlaces {
  private CovidPlaces() {}

  public static abstract class MainPlace extends ApplicationPlace {
    public MainPlace(final PlaceTokenizer<? extends MainPlace> tokenizer) {
      super(tokenizer);
    }
  }

  public static class MapPlace extends MainPlace {
    public static final DataType DEFAULT_DATA_TYPE = DataType.ACTIVE_CASES;
    private DataType type;

    public MapPlace() {
      super(CovidTokenizers.MAP);
    }

    @Override
    public String toString() {
      return "MapPlace [type=" + type + "]";
    }

    public MapPlace(final DataType type) {
      super(CovidTokenizers.MAP);
      this.type = type;
    }

    public DataType getType() {
      return type;
    }

    public void setType(final DataType type) {
      this.type = type;
    }

  }

  public static class GraphPlace extends MainPlace {
    public GraphPlace() {
      super(CovidTokenizers.GRAPH);
    }
  }

  public static class InfoPlace extends MainPlace {
    public InfoPlace() {
      super(CovidTokenizers.INFO);
    }
  }
}
