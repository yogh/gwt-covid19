package nl.yogh.gwt.wui.covid.layers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.geo.domain.IsLayer;

import ol.Collection;
import ol.Feature;
import ol.MapBrowserEvent;
import ol.OLFactory;
import ol.format.Wkt;
import ol.format.WktReadOptions;
import ol.layer.Layer;
import ol.layer.VectorLayerOptions;
import ol.proj.Projection;
import ol.source.Vector;
import ol.source.VectorOptions;
import ol.style.Style;

public abstract class AreaHighlightLayer<T, V> implements IsLayer<Layer> {
  private static final String PREFIX = "area-";

  private final ol.layer.Vector vectorLayer;
  private final Projection projection;

  private final Map<String, Feature> areaFeature = new HashMap<>();

  private final Vector vectorSource;
  private final VectorOptions vectorSourceOptions;

  private final Collection<Feature> features;

  private Consumer<T> selectConsumer;
  private Consumer<T> hoverConsumer;

  private final Map<String, T> areaMap = new HashMap<>();

  private Map<String, V> highlights;

  protected boolean activeAmounts;

  public AreaHighlightLayer(final ol.Map map, final Projection projection, final EventBus eventBus) {
    this.projection = projection;

    final VectorLayerOptions vectorLayerOptions = OLFactory.createOptions();
    vectorLayer = new ol.layer.Vector(vectorLayerOptions);
    vectorLayer.setZIndex(10001);

    features = new Collection<Feature>();

    vectorSourceOptions = OLFactory.createOptions();
    vectorSourceOptions.setFeatures(features);

    vectorSource = new Vector(vectorSourceOptions);

    // Update layer with new source
    vectorLayer.setSource(vectorSource);

    map.on("pointermove", (final MapBrowserEvent event) -> {
      if (!map.getLayers().contains(vectorLayer)) {
        return;
      }

      map.forEachFeatureAtPixel(event.getPixel(), feature -> {
        if (feature == null) {
          hoverConsumer.accept(null);
        } else {
          final String area = feature.getId().substring(PREFIX.length());
          hoverConsumer.accept(areaMap.get(area));
        }
        return null;
      });
    });

    map.on("click", (final MapBrowserEvent event) -> {
      if (!map.getLayers().contains(vectorLayer)) {
        return;
      }

      map.forEachFeatureAtPixel(event.getPixel(), feature -> {
        if (feature == null) {
          selectConsumer.accept(null);
        } else {

          final String area = feature.getId().substring(PREFIX.length());
          selectConsumer.accept(areaMap.get(area));
        }
        return null;
      });
    });
  }

  public void setAreas(final List<T> areas) {
    features.clear();
    areaMap.clear();
    areas.forEach(area -> {
      final String key = getKey(area);

      areaMap.put(key, area);

      // Create the final hexagon feature
      final Feature feat = createArea(getGeometry(area));
      feat.setId(PREFIX + key);

      final Style style = createStyle(null, null);
      feat.setStyle(style);

      areaFeature.put(key, feat);

      // Push the features
      features.push(feat);
    });
  }

  protected abstract Style createStyle(T t, V v);

  protected abstract String getGeometry(final T area);

  protected abstract String getKey(T area);

  public void displayAreaHighlight(final Map<String, V> highlights) {
    this.highlights = highlights;
    areaFeature.forEach((areaCode, feature) -> {
      final V virusInfo = highlights.get(areaCode);
      final T areaInfo = areaMap.get(areaCode);

      final Style style = createStyle(areaInfo, virusInfo);
      feature.setStyle(style);
      feature.changed();
    });
  }

  private Feature createArea(final String wktString) {
    final Wkt wkt = new Wkt();
    final WktReadOptions wktOptions = OLFactory.createOptions();
    wktOptions.setDataProjection(projection);
    wktOptions.setFeatureProjection(projection);
    return wkt.readFeature(wktString, wktOptions);
  }

  @Override
  public Layer asLayer() {
    return vectorLayer;
  }

  public void setSelectedConsumer(final Consumer<T> consumer) {
    this.selectConsumer = consumer;
  }

  public void setHoverConsumer(final Consumer<T> consumer) {
    this.hoverConsumer = consumer;
  }

  public void redraw() {
    displayAreaHighlight(highlights);
  }
}
