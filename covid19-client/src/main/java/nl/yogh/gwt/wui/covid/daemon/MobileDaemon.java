package nl.yogh.gwt.wui.covid.daemon;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.binder.EventBinder;
import com.google.web.bindery.event.shared.binder.EventHandler;

import io.yogh.gwt.wui.event.BasicEventComponent;

import nl.yogh.gwt.wui.covid.command.ToggleMenuCommand;
import nl.yogh.gwt.wui.covid.command.ToggleSettingsCommand;

public class MobileDaemon extends BasicEventComponent {
  private static final MobileDaemonEventBinder EVENT_BINDER = GWT.create(MobileDaemonEventBinder.class);

  interface MobileDaemonEventBinder extends EventBinder<MobileDaemon> {}

  private boolean compactActive;

  public MobileDaemon() {
    Window.addResizeHandler(e -> {
      onResize();
    });
    onResize();
  }

  @EventHandler
  public void onToggleNavigationCommand(final ToggleMenuCommand c) {
    if (compactActive) {
      c.silence();
    }
  }

  @EventHandler
  public void onToggleSettingsCommand(final ToggleSettingsCommand c) {
    if (compactActive) {
      c.silence();
    }
  }

  private void onResize() {
    compactActive = Window.getClientWidth() > 800;
  }

  @Override
  public void setEventBus(final EventBus eventBus) {
    super.setEventBus(eventBus, this, EVENT_BINDER);
  }
}
