package nl.yogh.gwt.wui.covid.command;

import io.yogh.gwt.wui.command.SimpleGenericCommand;

import nl.yogh.covid.domain.IndexedHospitalisationDistributionSet;
import nl.yogh.gwt.wui.covid.event.HospitalisationDistributionSetChangeEvent;

public class HospitalisationDistributionSetChangeCommand
    extends SimpleGenericCommand<IndexedHospitalisationDistributionSet, HospitalisationDistributionSetChangeEvent> {
  public HospitalisationDistributionSetChangeCommand(final IndexedHospitalisationDistributionSet value) {
    super(value);
  }

  @Override
  protected HospitalisationDistributionSetChangeEvent createEvent(final IndexedHospitalisationDistributionSet value) {
    return new HospitalisationDistributionSetChangeEvent(value);
  }
}
