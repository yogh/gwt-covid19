package nl.yogh.gwt.wui.covid.event;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

import nl.yogh.covid.domain.IndexedCasesDistributionSet;

public class ActiveCasesDistributionSetChangeEvent extends SimpleGenericEvent<IndexedCasesDistributionSet> {
  public ActiveCasesDistributionSetChangeEvent(final IndexedCasesDistributionSet value) {
    super(value);
  }
}
