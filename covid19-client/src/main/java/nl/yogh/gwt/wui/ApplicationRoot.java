package nl.yogh.gwt.wui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.vue.AcceptsOneComponent;
import io.yogh.gwt.wui.activity.ActivityManager;
import io.yogh.gwt.wui.daemon.DaemonBootstrapper;
import io.yogh.gwt.wui.dev.DevelopmentObserver;
import io.yogh.gwt.wui.dev.GWTProd;
import io.yogh.gwt.wui.event.RequestClientLoadFailureEvent;
import io.yogh.gwt.wui.event.RequestConnectionFailureEvent;
import io.yogh.gwt.wui.event.RequestServerLoadFailureEvent;
import io.yogh.gwt.wui.history.HistoryManager;
import io.yogh.gwt.wui.init.ExceptionalPirateCache;
import io.yogh.gwt.wui.init.Initializer;
import io.yogh.gwt.wui.main.util.NotificationUtil;
import io.yogh.gwt.wui.service.exception.RequestBlockedException;
import io.yogh.gwt.wui.service.exception.RequestClientException;
import io.yogh.gwt.wui.service.exception.RequestServerException;
import io.yogh.gwt.wui.util.WebUtil;

import nl.yogh.gwt.wui.covid.component.error.ApplicationErrorView;
import nl.yogh.gwt.wui.covid.component.error.ApplicationErrorViewFactory;

/**
 * Root of the application logic.
 */
public class ApplicationRoot {
  @SuppressWarnings("unused") @Inject private DevelopmentObserver development;

  @Inject private HistoryManager historyManager;

  @Inject private EventBus eventBus;

  @Inject DaemonBootstrapper daemonBootstrapper;

  @Inject ActivityManager<AcceptsOneComponent> activityManager;

  @Inject Initializer initializer;

  @Inject VueRootViewFactory rootViewFactory;

  private VueRootView rootView;

  /**
   * Starts the application.
   */
  public void startUp(final Runnable finish) {
    final String root = Document.get().getElementsByTagName("base").getItem(0).getAttribute("href");
    WebUtil.setAbsoluteRoot(root);

    setUncaughtExceptionHandler();

    daemonBootstrapper.setEventBus(eventBus);

    rootView = rootViewFactory.create();
    activityManager.setPanel(rootView);

    initializer.init(() -> this.onFinishStartup(finish), e -> this.hideDisplay(e, finish));
  }

  private void onFinishStartup(final Runnable finish) {
    finish.run();
    rootView.vue().$mount("#base");
    historyManager.handleCurrentHistory();
  }

  /**
   * Hides the main application display, if attached.
   */
  public void hideDisplay(final Throwable e, final Runnable finish) {
    finish.run();
    GWTProd.error("Aborting startup due to fatal error: " + e.getMessage());
    final ApplicationErrorView err = ApplicationErrorViewFactory.get().create();
    err.setError(e);
    err.vue().$mount("#error");
  }


  private void setUncaughtExceptionHandler() {
    GWT.setUncaughtExceptionHandler(e -> {
      final Throwable cause = findCause(e);

      if (cause instanceof RequestClientException) {
        eventBus.fireEvent(new RequestClientLoadFailureEvent(true));
      } else if (cause instanceof RequestServerException) {
        eventBus.fireEvent(new RequestServerLoadFailureEvent(true));
      } else if (cause instanceof RequestBlockedException) {
        eventBus.fireEvent(new RequestConnectionFailureEvent(true));
      } else {
        NotificationUtil.broadcastError(eventBus, cause);
      }
    });
  }

  private Throwable findCause(final Throwable e) {
    final Throwable cause;

    final Throwable booty = ExceptionalPirateCache.pop();
    if (booty == null) {
      if (e == null) {
        cause = null;
      } else if (e.getCause() == null) {
        cause = e;
      } else {
        // Recurse down
        cause = findCause(e.getCause());
      }
    } else {
      // Recurse on the hidden exception
      cause = findCause(booty);
    }

    return cause;
  }
}
