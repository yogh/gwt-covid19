package nl.yogh.gwt.wui.covid.command;

import io.yogh.gwt.wui.command.SimpleCommand;

import nl.yogh.gwt.wui.covid.event.ToggleApplicationMenuEvent;

public class ToggleApplicationMenuCommand extends SimpleCommand<ToggleApplicationMenuEvent> {
  private boolean open;

  public void resolve(final boolean open) {
    this.open = open;
  }

  @Override
  protected ToggleApplicationMenuEvent createEvent() {
    return new ToggleApplicationMenuEvent(open);
  }
}
