package nl.yogh.gwt.wui;

import com.google.inject.Inject;

import io.yogh.gwt.wui.dev.GWTProd;

import nl.yogh.gwt.wui.covid.ApplicationGinjector;
import nl.yogh.gwt.wui.resources.R;

public class ApplicationImpl extends Application {
  @Inject private ApplicationRoot appRoot;

  @Override
  public void create(final Runnable finish) {
    R.init();

    ApplicationGinjector.INSTANCE.inject(this);

    onFinishedLoading(finish);
  }

  /**
   * Initializes the application activity managers, user interface, and starts the application by handling the current history token.
   * @param finish 
   */
  private void onFinishedLoading(final Runnable finish) {
    try {
      appRoot.startUp(finish);
    } catch (final Exception e) {
      GWTProd.log("Failure while starting up.");
      e.printStackTrace();
      appRoot.hideDisplay(e, finish);
      throw e;
    }
  }
}
