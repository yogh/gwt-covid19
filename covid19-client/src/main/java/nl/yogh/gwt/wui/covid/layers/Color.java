package nl.yogh.gwt.wui.covid.layers;

import io.yogh.gwt.wui.dev.GWTProd;

import ol.OLFactory;

public final class Color {

//function HSVtoRGB(final h, s, v) {
//    var r, g, b, i, f, p, q, t;
//    if (arguments.length === 1) {
//        s = h.s, v = h.v, h = h.h;
//    }
//    i = Math.floor(h * 6);
//    f = h * 6 - i;
//    p = v * (1 - s);
//    q = v * (1 - f * s);
//    t = v * (1 - (1 - f) * s);
//    switch (i % 6) {
//        case 0: r = v, g = t, b = p; break;
//        case 1: r = q, g = v, b = p; break;
//        case 2: r = p, g = v, b = t; break;
//        case 3: r = p, g = q, b = v; break;
//        case 4: r = t, g = p, b = v; break;
//        case 5: r = v, g = p, b = q; break;
//    }
//    return {
//        r: Math.round(r * 255),
//        g: Math.round(g * 255),
//        b: Math.round(b * 255)
//    };
//}

  /**
   * <a href=
   * "http://stackoverflow.com/questions/7896280/converting-from-hsv-hsb-in-java-to-rgb-without-using-java-awt-color-disallowe">
   * Converting from HSV (HSB in Java) to RGB without using java.awt.Color</a>
   */
  public static Color fromHSV(final float h,
      final float s,
      final float v) {

    GWTProd.log("");
    GWTProd.log("Hue: " + h);

    final int i = (int) Math.floor(h * 6);
    final float f = i * 6 - h;
    final float p = v * (1 - s);
    final float q = v * (1 - f * s);
    final float t = v * (1 - (1 - f) * s);

    GWTProd.log("h: " + h);
    GWTProd.log("t: " + t + ":> " + p + " > " + v);

    switch (i % 6) {
    case 0:
      return rgbToString(v, t, p);
    case 1:
      return rgbToString(q, v, p);
    case 2:
      return rgbToString(p, v, t);
    case 3:
      return rgbToString(p, q, v);
    case 4:
      return rgbToString(t, p, v);
    case 5:
      return rgbToString(v, p, q);
    default:
      throw new RuntimeException(
          "Could not convert from HSV (" + h + ", " + s + ", " + v + ") to RGB");
    }
  }

  public static Color rgbToString(final float r, final float g, final float b) {
    GWTProd.log("RGB: " + r + " > " + g + " > " + b);
    return new Color((int) (r * 255 + 0.5),
        (int) (g * 255 + 0.5),
        (int) (b * 255 + 0.5));
  }

  private final int r, g, b;

  private Color(final int r, final int g, final int b) {
    if (r < 0) {
      this.r = 0;
    } else if (r > 255) {
      this.r = 255;
    } else {
      this.r = r;
    }

    if (g < 0) {
      this.g = 0;
    } else if (g > 255) {
      this.g = 255;
    } else {
      this.g = g;
    }

    if (b < 0) {
      this.b = 0;
    } else if (b > 255) {
      this.b = 255;
    } else {
      this.b = b;
    }
  }

  public ol.color.Color asColor() {
    return OLFactory.createColor(r, g, b, 0.9);
  }
}
