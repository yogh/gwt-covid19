package nl.yogh.gwt.wui.covid.event;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

import nl.yogh.covid.domain.IndexedHospitalisationDistributionSet;

public class ActiveHospitalisationDistributionSetChangeEvent extends SimpleGenericEvent<IndexedHospitalisationDistributionSet> {
  public ActiveHospitalisationDistributionSetChangeEvent(final IndexedHospitalisationDistributionSet value) {
    super(value);
  }
}
