package nl.yogh.gwt.wui.covid.mapper;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;

import io.yogh.gwt.vue.AcceptsOneComponent;
import io.yogh.gwt.wui.activity.Activity;
import io.yogh.gwt.wui.activity.ActivityMapper;
import io.yogh.gwt.wui.place.ApplicationPlace;
import io.yogh.gwt.wui.place.DefaultPlace;
import io.yogh.gwt.wui.place.Place;

import nl.yogh.gwt.wui.covid.factory.CovidActivityFactory;
import nl.yogh.gwt.wui.covid.place.CovidPlaces.MainPlace;

public class CovidActivityMapper implements ActivityMapper<AcceptsOneComponent> {
  private final CovidActivityFactory factory;

  @Inject
  public CovidActivityMapper(@DefaultPlace final ApplicationPlace place, final CovidActivityFactory factory) {
    this.factory = factory;
  }

  @Override
  public Activity<?, AcceptsOneComponent> getActivity(final Place place) {
    Activity<?, AcceptsOneComponent> presenter = null;

    presenter = tryGetActivity(place);

    if (presenter == null) {
      GWT.log("Presenter is null: Place ends up nowhere. " + place);
      throw new RuntimeException("No Presenter found for place " + place);
    }

    return presenter;
  }

  private Activity<?, AcceptsOneComponent> tryGetActivity(final Place place) {
    Activity<?, AcceptsOneComponent> presenter = null;

    if (place instanceof MainPlace) {
      presenter = factory.createMainPresenter((MainPlace) place);
    }

    return presenter;
  }
}
