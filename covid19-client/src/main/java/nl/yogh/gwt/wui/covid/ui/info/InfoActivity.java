package nl.yogh.gwt.wui.covid.ui.info;

import javax.inject.Inject;

import com.google.inject.assistedinject.Assisted;

import nl.yogh.gwt.wui.covid.ui.MainView;

public class InfoActivity implements InfoPresenter {
  @Inject
  public InfoActivity(@Assisted final MainView view) {
    view.setMain(InfoViewFactory.get());
  }

  @Override
  public void setView(final InfoView view) {}
}
