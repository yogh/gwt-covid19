package nl.yogh.gwt.wui.covid.layers;

import com.google.web.bindery.event.shared.EventBus;

import ol.Map;
import ol.proj.Projection;

import nl.yogh.covid.domain.MunicipalityInfo;

public abstract class MunicipalityLayer<V> extends AreaHighlightLayer<MunicipalityInfo, V> {
  public MunicipalityLayer(final Map map, final Projection projection, final EventBus eventBus) {
    super(map, projection, eventBus);
  }

  @Override
  protected String getGeometry(final MunicipalityInfo area) {
    return area.geometry();
  }

  @Override
  protected String getKey(final MunicipalityInfo area) {
    return area.code();
  }
}
