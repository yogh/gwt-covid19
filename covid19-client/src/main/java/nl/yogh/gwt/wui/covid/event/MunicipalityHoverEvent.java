package nl.yogh.gwt.wui.covid.event;

import io.yogh.gwt.wui.event.SimpleGenericEvent;

import nl.yogh.covid.domain.MunicipalityInfo;

public class MunicipalityHoverEvent extends SimpleGenericEvent<MunicipalityInfo> {
  public MunicipalityHoverEvent(final MunicipalityInfo code) {
    super(code);
  }
}
