package nl.yogh.gwt.wui.covid.factory;

import nl.yogh.gwt.wui.covid.place.CovidPlaces.MainPlace;
import nl.yogh.gwt.wui.covid.ui.MainActivity;

public interface CovidActivityFactory {
  MainActivity createMainPresenter(MainPlace place);
}
