package nl.yogh.gwt.wui.covid.layers;

import com.google.web.bindery.event.shared.EventBus;

import ol.OLFactory;
import ol.proj.Projection;
import ol.style.Style;
import ol.style.StyleOptions;

import nl.yogh.covid.domain.ProvinceInfo;
import nl.yogh.covid.domain.VirusInfo;

public class ProvinceCasesLayer extends ProvinceLayer<VirusInfo> {
  private static final int CASES_START = 25;
  private static final int LOG_MULTIPLIER = 15;
  private static final int MULTIPLIER = 10;

  private static final int LOG_CASES_PER_START = 50;
  private static final int RELATIVE_LOG_MULTIPLIER = 25;
  private static final int PER_MULTIPLIER = 5;

  private boolean isRelativeStyling = true;

  public ProvinceCasesLayer(final ol.Map map, final Projection projection, final EventBus eventBus) {
    super(map, projection, eventBus);
  }

  @Override
  protected Style createStyle(final ProvinceInfo provinceInfo, final VirusInfo virusInfo) {
    if (isRelativeStyling) {
      return createStylePer(provinceInfo, virusInfo);
    } else {
      return createStyleAbsolute(virusInfo);
    }
  }

  private Style createStyleAbsolute(final VirusInfo info) {
    final StyleOptions styleOptions = new StyleOptions();
    styleOptions.setStroke(OLFactory.createStroke(OLFactory.createColor(214, 51, 39, 0.1), 2));

    if (info != null) {
      final int r = info.amount() == 0 ? 0 : info.amount() > CASES_START ? 255 - logScale(info.amount() - CASES_START, LOG_MULTIPLIER) : 255;
      final int g = Math.max(0, 255 - info.amount() * MULTIPLIER);
      final int b = 0;

      final double a = info.amount() == 0 ? 0.1 : 0.9;
      styleOptions.setFill(OLFactory.createFill(OLFactory.createColor(r, g, b, a)));
    }

    return new Style(styleOptions);
  }

  private Style createStylePer(final ProvinceInfo province, final VirusInfo virusInfo) {
    final StyleOptions styleOptions = new StyleOptions();
    styleOptions.setStroke(OLFactory.createStroke(OLFactory.createColor(214, 51, 39, 0.1), 2));

    if (virusInfo != null) {
      final double cases = virusInfo.amount();
      final double casesPerThousand = cases / (province.population() / (100 * 1000D));

      final int r = casesPerThousand == 0 ? 0
          : casesPerThousand > LOG_CASES_PER_START ? 255 - logScale(casesPerThousand - LOG_CASES_PER_START, RELATIVE_LOG_MULTIPLIER) : 255;
      final int g = (int) Math.max(0, 255 - casesPerThousand * PER_MULTIPLIER);
      final int b = 0;

      final double a = virusInfo.amount() == 0 ? 0.1 : 0.9;
      styleOptions.setFill(OLFactory.createFill(OLFactory.createColor(r, g, b, a)));
    }

    return new Style(styleOptions);
  }

  private int logScale(final double casesPerThousand, final int multiplier) {
    final double logg = Math.log(casesPerThousand) * multiplier;

    return (int) logg;
  }

  public void setIsRelativeStyling(final boolean isRelativeStyling) {
    this.isRelativeStyling = isRelativeStyling;
  }
}
