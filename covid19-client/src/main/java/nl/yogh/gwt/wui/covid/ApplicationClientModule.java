package nl.yogh.gwt.wui.covid;

import com.google.gwt.inject.client.AbstractGinModule;
import com.google.gwt.inject.client.assistedinject.GinFactoryModuleBuilder;
import com.google.gwt.place.shared.PlaceHistoryHandler.Historian;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.web.bindery.event.shared.EventBus;

import io.yogh.gwt.geo.epsg.EPSG;
import io.yogh.gwt.geo.epsg.EPSGRDNew;
import io.yogh.gwt.vue.AcceptsOneComponent;
import io.yogh.gwt.vue.activity.VueActivityManager;
import io.yogh.gwt.wui.activity.ActivityManager;
import io.yogh.gwt.wui.activity.ActivityMapper;
import io.yogh.gwt.wui.daemon.DaemonBootstrapper;
import io.yogh.gwt.wui.dev.DevelopmentObserver;
import io.yogh.gwt.wui.event.CommandEventBus;
import io.yogh.gwt.wui.history.HTML5Historian;
import io.yogh.gwt.wui.history.PlaceHistoryMapper;
import io.yogh.gwt.wui.place.ApplicationPlace;
import io.yogh.gwt.wui.place.DefaultPlace;

import nl.yogh.gwt.wui.covid.daemon.CovidDaemonBootstrapper;
import nl.yogh.gwt.wui.covid.dev.CovidDevelopmentObserver;
import nl.yogh.gwt.wui.covid.factory.CovidActivityFactory;
import nl.yogh.gwt.wui.covid.mapper.CovidActivityMapper;
import nl.yogh.gwt.wui.covid.mapper.CovidPlaceHistoryMapper;
import nl.yogh.gwt.wui.covid.place.CovidPlaces.MapPlace;
import nl.yogh.gwt.wui.covid.ui.MainActivityFactory;

public class ApplicationClientModule extends AbstractGinModule {
  @Override
  protected void configure() {
    bind(EventBus.class).to(CommandEventBus.class).in(Singleton.class);
    bind(EPSG.class).to(EPSGRDNew.class);

    bind(ApplicationPlace.class).annotatedWith(DefaultPlace.class).to(MapPlace.class);
    bind(Historian.class).to(HTML5Historian.class);

    bind(new TypeLiteral<ActivityMapper<AcceptsOneComponent>>() {}).to(CovidActivityMapper.class);
    bind(new TypeLiteral<ActivityManager<AcceptsOneComponent>>() {}).to(VueActivityManager.class);
    bind(PlaceHistoryMapper.class).to(CovidPlaceHistoryMapper.class);
    bind(DaemonBootstrapper.class).to(CovidDaemonBootstrapper.class);
    bind(DevelopmentObserver.class).to(CovidDevelopmentObserver.class);

    install(new GinFactoryModuleBuilder().build(CovidActivityFactory.class));
    install(new GinFactoryModuleBuilder().build(MainActivityFactory.class));
  }
}
