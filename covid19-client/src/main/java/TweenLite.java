import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;
import jsinterop.base.JsPropertyMap;

import elemental2.core.JsObject;

@JsType(isNative = true, namespace = JsPackage.GLOBAL)
public class TweenLite {
  public static native String to(JsPropertyMap $data, double duration, JsObject obj);
}
