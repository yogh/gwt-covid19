### Covid Web application image

The webapp war should be placed in this directory as `covid.war` while building the image.

##### Example build
```shell
docker build -t covid19-server:latest .
```

##### Example run
```shell
docker run --rm --network host \
  -p 8080:8080 \
  covid19-server:latest
```
