docker run --rm \
  --network host \
  --name covid19-server \
  --hostname covid-server \
  -d \
  covid-server:latest
