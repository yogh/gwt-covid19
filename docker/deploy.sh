# Build artifacts
cd ..
if [ "$1" = "rebuild" ]; then
mvn clean
mvn clean install
fi
cd docker

# Build images
cd covid-services
sh fetch.sh
sh build.sh
cd ../covid-server
sh build.sh
cd ../apache
sh build.sh
cd ..

# Stop existing
sh stop.sh

# Deploy containers
cd covid-services
sh deploy.sh
cd ../covid-server
sh deploy.sh
cd ../apache
sh deploy.sh
