docker run --rm \
  --network host \
  --name covid19-services \
   -e JAVA_OPTS='-Xmx1g' \
  -v $(pwd)/config.yml:/config.yml \
  -v $(pwd)/cases:/cases \
  -v $(pwd)/fatalities:/fatalities \
  -v $(pwd)/hospitalisations:/hospitalisations \
  -v $(pwd)/Gemeentegrenzen.gml:/Gemeentegrenzen.gml \
  -v $(pwd)/Provinciegrenzen.gml:/Provinciegrenzen.gml \
  -v $(pwd)/Gemeenten_alfabetisch_2019.csv:/Gemeenten_alfabetisch_2019.csv \
  -v $(pwd)/Populations.csv:/Populations.csv \
  -d \
  covid-services:latest
