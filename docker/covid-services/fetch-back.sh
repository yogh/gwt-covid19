#rm -rf cases/*
#rm -rf hospitalisations/*
#rm -rf fatalities/*

cp cases/* ../../scripts/conf/cases/
cp hospitalisations/* ../../scripts/conf/hospitalisations/
cp fatalities/* ../../scripts/conf/fatalities/
