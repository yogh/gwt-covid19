wget -N https://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/extract/bestuurlijkegrenzen.zip?datum=2020-01-10 --content-disposition

unzip -o bestuurlijkegrenzen.zip Gemeentegrenzen.gml
unzip -o bestuurlijkegrenzen.zip Provinciegrenzen.gml

rm -rf cases/*
rm -rf hospitalisations/*
rm -rf fatalities/*

cp ../../scripts/conf/cases/* cases/
cp ../../scripts/conf/hospitalisations/* hospitalisations/
cp ../../scripts/conf/fatalities/* fatalities/
cp ../../scripts/conf/Gemeenten_alfabetisch_2019.csv Gemeenten_alfabetisch_2019.csv
cp ../../scripts/conf/Regionale_kerncijfers_Nederland.csv Populations.csv
