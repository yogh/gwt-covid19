### Covid API (Services) application image

The API jar should be placed in this directory as `app.jar` while building the image.

The configuration can be provided by using a config file (volume) or by using environment variables.

An example configuration can be found as `config.example.yml`.
While starting the container use a volume and make it available under `/config.yml`.

##### Example build
```shell
docker build -t covid-services:latest .
```

##### Example run (with config file)
```shell
docker run --rm --network host \
  -v $(pwd)/config.yml:/config.yml \
  -p 8080:8080 \
  covid-services:latest
```
