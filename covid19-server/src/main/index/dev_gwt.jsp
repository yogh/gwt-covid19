<!doctype html>
<html style="height:100vh;display:flex;" lang="nl">
<head>
  <title>(DEV RLD) COVID19 Tracker</title>

  <base href="${pageContext.request.contextPath}/">

  <meta http-equiv="content-type" content="text/html;charset=utf-8">
  <meta name="gwt:property" content="locale=nl">
  <meta name="gwt:property" content="compiler.stackMode=native" />
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=1.4, minimum-scale=0.7">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  
  <!-- Third party dependencies -->
  <link rel="stylesheet" href="res/ol.css" type="text/css">
  <script src="webjars/openlayers/${openlayers.version}/ol.js" type="text/javascript"></script>
  <script src="webjars/vue/${vue.version}/vue.js" type="text/javascript"></script>
  <script src="webjars/gsap-js/${gsap-js.version}/minified/TweenMax.min.js" type="text/javascript"></script>
  
  <!-- Dev scripts -->
  <script src="scripts/reload.js"></script>
  <script src="scripts/config_dev.js"></script>
  
  <!-- Application code -->
  <script src="application/application.nocache.js"></script>
</head>
<body style="display:flex;flex-grow:1;margin:0px;height:100vh;">
  <div id="bootstrap"></div>
  <div id="base"></div>
  <div id="error"></div>
</body>
</html>
